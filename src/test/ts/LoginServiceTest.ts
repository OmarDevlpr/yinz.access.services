// libs
import * as assert from "assert";
import { Chance } from "chance";
import Container from "@yinz/container/ts/Container";
import { TypeormDao, TypeormDataSource } from "@yinz/commons.data";
import { YinzConnection, YinzTransaction } from "@yinz/commons.data/ts/DataSource";
import Authz from "@yinz/access.data/models/Authz";
import Profile from "@yinz/access.data/models/Profile";
import User from "@yinz/access.data/models/User";
import LoginLog from "@yinz/access.data/models/LoginLog";
import LoginService from "../../main/ts/LoginService";
import {PasswordUtils} from "@yinz/commons";
import ProfileAuthz from "@yinz/access.data/models/ProfileAuthz";


const container = new Container({
    paramsFile: 'params.yml',
    modules: [
        '@yinz/commons/ts',
        '@yinz/commons.data/ts',
        '@yinz/access.data/ts',
        '@yinz/commons.services/ts',
        process.cwd() + '/dist/main/ts'
    ],
});

const chance = new Chance();
// const Exception = container.getClazz('Exception');

const dataSource = container.getBean<TypeormDataSource>('typeormDataSource');


const authzDao    = container.getBean<TypeormDao<Authz>>('authzDao');
const profileAuthzDao = container.getBean<TypeormDao<ProfileAuthz>>('profileAuthzDao');
const userDao     = container.getBean<TypeormDao<User>>('userDao');
const profileDao  = container.getBean<TypeormDao<Profile>>('profileDao');
const loginLogDao = container.getBean<TypeormDao<LoginLog>>('loginLogDao');

const loginservice = container.getBean<LoginService>("loginService");


let conn: YinzConnection;
let trans: YinzTransaction;



describe('| access.services <LoginService>', function () {

    before(() => {
        return new Promise(async (resolve) => {
            conn = await dataSource.getConn();
            resolve(conn);
        })
    });

    after(() => {
        return new Promise(async (resolve) => {
            await dataSource.relConn(conn)
            resolve(true);
        })
    });

    beforeEach(() => {
        return new Promise(async (resolve) => {
            trans = await dataSource.startTrans(conn);
            resolve(trans);
        })
    });

    afterEach(() => {
        return new Promise(async (resolve) => {
            await dataSource.rollbackTrans(trans)
            resolve(true);
        })
    });

    // !! 30 sec perf boost
    // it('performance test Sync  ~~~!!!!~~~~', async () => {        

    //     const {
    //         performance
    //     } = require('perf_hooks');

    //     let req = {
    //         reqUser: chance.string({ length: 10 }), 
    //         reqRef: chance.string({ length: 10 }),
    //         password: chance.string({ length: 10 }),
    //     }

    //     let  t0 = performance.now();
         
    //     for ( let x = 0; x < 10000; x++) {
    //         await loginservice.process(trans, req, {user: "__super_user__"})
    //     }

    //     let t1 = performance.now();
    //     console.log("Call to loginService Sync took " + (t1 - t0) + " milliseconds.");

    // })

    // it('performance test Async ~~~!!!!~~~~', async () => {
    //     const {
    //         performance
    //     } = require('perf_hooks');

    //     let req = {
    //         reqUser: chance.string({ length: 10 }),
    //         reqRef: chance.string({ length: 10 }),
    //         password: chance.string({ length: 10 }),
    //     }

    //     let promises: Promise<any>[] = [];

    //     let t0 = performance.now();

    //     for (let x = 0; x < 10000; x++) {
    //         promises.push(loginservice.process(trans, req, { user: "__super_user__" }))
    //     }

    //     await Promise.all(promises);

    //     let t1 = performance.now();
    //     console.log("Call to loginService ASync took " + (t1 - t0) + " milliseconds.");
    //     process.exit(0)
    // })


    it('\n' +
    '       | Conditions: \n' +
    '       | - userCode is valid \n' +
    '       | - password is valid \n' +
    '       | - user.wrongAttempts is 0  \n' +
    '       | - user.status is A(ctive)  \n' +
    '       | - maxWrongAttempts is 3  \n' +
    '       | Expected :  \n' +
    '       | -> should return rspCode A(ccepted).  \n' +
    '', async () => {

        // 1. prepare test-data 

        // 1.1 create active user

        let password = chance.string({ length: 10 });

        let user = new User();
        user.code = chance.string({ length: 10 });
        user.password = await PasswordUtils.hashPassword(password, user.code);
        user.wrongAttempts = 0;
        user.status = "A";
        


        // 1.1.1 create profile
        let profile = new Profile();
        profile.code = chance.string({ length: 10 });    
        profile = await profileDao.create(trans, profile, {user: "__super_user__"});

        // 1.1.2 create authz
        let authz = new Authz();
        authz.code = "LoginService";
        authz = await authzDao.create(trans, authz, { user: '__super_user__' });

        // 1.1.3 link between them
        profileAuthzDao.create(trans, {profile, authz, allowExecute: true}, {user: '__super_user__', include: ['profile', 'authz']})

        // 1.1.4 create user
        user.profiles = [profile];
        user = await userDao.create(trans, user, { include: ['profiles'], user: '__super_user__' });

        // 1.2 preapre request data

        let req = {
            reqUser: user.code,
            reqRef : chance.string({length: 10}),
            password: password
        }
        
        let options = {
            user: req.reqUser
        };

        // 2. execute test

        let rsp = await loginservice.process(trans, req, options);

        // 3. compare outcome

        // 3.1. check the returned response        
        assert.ok(rsp);
        assert.strictEqual(rsp.rspCode, 'A');
        
        assert.strictEqual(rsp.user.code, req.reqUser);
        assert.strictEqual(rsp.user.wrongAttempts, 0);
        assert.strictEqual(rsp.user.status, 'A');

        // 3.2. check the user's wrongAttempts and status

        let userFindResult = (await userDao.findByFilter(trans, {code: req.reqUser}, { user: "__super_user__"}))[0];

        assert.ok           (userFindResult);
        assert.strictEqual  (userFindResult.code            , req.reqUser);
        assert.strictEqual  (userFindResult.wrongAttempts   , 0);
        assert.strictEqual  (userFindResult.status          , 'A');

        // 3. check that request was logged

        let logFindResult = await loginLogDao.findByFilter(trans, { reqRef: req.reqRef }, { user: "__super_user__" });
        
        assert.ok           (logFindResult);
        assert.strictEqual  (logFindResult.length, 1);
        assert.strictEqual  (logFindResult[0].rspCode, 'A');
        
        assert.ok           (logFindResult[0].password !== password);
        assert.strictEqual  (logFindResult[0].password.length, 60);

    });


    it('\n' +
        '       | Conditions: \n' +
        '       | - userCode is valid \n' +
        '       | - password is valid \n' +
        '       | - user.wrongAttempts is 2  \n' +
        '       | - user.status is A(ctive)  \n' +
        '       | - maxWrongAttempts is 3  \n' +
        '       | Expected :  \n' +
        '       | -> should return rspCode A(ccepted).  \n' +
        '       | -> reset wrong attempts to 0.  \n' +
        '', async () => {

        // 1. prepare test-data 

        // 1.1 create active user

        let password = chance.string({ length: 10 });

        let user = new User();
        user.code = chance.string({ length: 10 });
        user.password = await PasswordUtils.hashPassword(password, user.code);
        user.wrongAttempts = 2;
        user.status = "A";


        // 1.1.1 create profile
        let profile = new Profile();
        profile.code = chance.string({ length: 10 });    
        profile = await profileDao.create(trans, profile, {user: "__super_user__"});

        // 1.1.2 create authz
        let authz = new Authz();
        authz.code = "LoginService";
        authz = await authzDao.create(trans, authz, { user: '__super_user__' });

        // 1.1.3 link between them
        profileAuthzDao.create(trans, {profile, authz, allowExecute: true}, {user: '__super_user__', include: ['profile', 'authz']})

        // 1.1.4 create user
        user.profiles = [profile];
        user = await userDao.create(trans, user, { include: ['profiles'], user: '__super_user__' });

        // 1.2 preapre request data

        let req = {
            reqUser: user.code,
            reqRef: chance.string({ length: 10 }),
            password: password
        }

        let options = {
            user: req.reqUser
        };

        // 2. execute test

        let rsp = await loginservice.process(trans, req, options);

        // 3. compare outcome

        // 3.1. check the returned response
        
        assert.ok(rsp);
        assert.strictEqual(rsp.rspCode, 'A');

        assert.strictEqual(rsp.user.code, req.reqUser);
        assert.strictEqual(rsp.user.wrongAttempts, 2);
        assert.strictEqual(rsp.user.status, 'A');

        // 3.2. check the user's wrongAttempts and status

        let userFindResult = (await userDao.findByFilter(trans, { code: req.reqUser }, { user: "__super_user__" }))[0];

        assert.ok(userFindResult);
        assert.strictEqual(userFindResult.code, req.reqUser);
        assert.strictEqual(userFindResult.wrongAttempts, 0);
        assert.strictEqual(userFindResult.status, 'A');

        // 3. check that request was logged

        let logFindResult = await loginLogDao.findByFilter(trans, { reqRef: req.reqRef }, { user: "__super_user__" });

        assert.ok(logFindResult);
        assert.strictEqual(logFindResult.length, 1);
        assert.strictEqual(logFindResult[0].rspCode, 'A');

        assert.ok(logFindResult[0].password !== password);
        assert.strictEqual(logFindResult[0].password.length, 60);

    });


    it('\n' +
        '       | Conditions: \n' +
        '       | - userCode is valid \n' +
        '       | - passwordDigest is valid \n' +
        '       | - user.wrongAttempts is 0  \n' +
        '       | - user.status is A(ctive)  \n' +
        '       | - maxWrongAttempts is 3  \n' +
        '       | Expected :  \n' +
        '       | -> should return rspCode A(ccepted).  \n' +
        '', async () => {

        // 1. prepare test-data 

        // 1.1 create active user

        let password = chance.string({ length: 10 });

        let user = new User();
        user.code = chance.string({ length: 10 });
        user.password = await PasswordUtils.hashPassword(password, user.code);
        user.wrongAttempts = 0;
        user.status = "A";


        // 1.1.1 create profile
        let profile = new Profile();
        profile.code = chance.string({ length: 10 });    
        profile = await profileDao.create(trans, profile, {user: "__super_user__"});

        // 1.1.2 create authz
        let authz = new Authz();
        authz.code = "LoginService";
        authz = await authzDao.create(trans, authz, { user: '__super_user__' });

        // 1.1.3 link between them
        profileAuthzDao.create(trans, {profile, authz, allowExecute: true}, {user: '__super_user__', include: ['profile', 'authz']})

        // 1.1.4 create user
        user.profiles = [profile];
        user = await userDao.create(trans, user, { include: ['profiles'], user: '__super_user__' });

        // 1.2 preapre request data

        let req = {
            reqUser: user.code,
            reqRef: chance.string({ length: 10 }),
            passwordDigest: user.password
        }

        let options = {
            user: req.reqUser
        };

        // 2. execute test

        let rsp = await loginservice.process(trans, req, options);

        // 3. compare outcome

        // 3.1. check the returned response        
        assert.ok(rsp);
        assert.strictEqual(rsp.rspCode, 'A');

        assert.strictEqual(rsp.user.code, req.reqUser);
        assert.strictEqual(rsp.user.wrongAttempts, 0);
        assert.strictEqual(rsp.user.status, 'A');

        // 3.2. check the user's wrongAttempts and status

        let userFindResult = (await userDao.findByFilter(trans, { code: req.reqUser }, { user: "__super_user__" }))[0];

        assert.ok(userFindResult);
        assert.strictEqual(userFindResult.code, req.reqUser);
        assert.strictEqual(userFindResult.wrongAttempts, 0);
        assert.strictEqual(userFindResult.status, 'A');

        // 3. check that request was logged

        let logFindResult = await loginLogDao.findByFilter(trans, { reqRef: req.reqRef }, { user: "__super_user__" });

        assert.ok(logFindResult);
        assert.strictEqual(logFindResult.length, 1);
        assert.strictEqual(logFindResult[0].rspCode, 'A');

        assert.ok(logFindResult[0].password !== password);
        assert.strictEqual(logFindResult[0].password.length, 60);

    });

    it('\n' +
        '       | Conditions: \n' +
        '       | - userCode is valid \n' +
        '       | - passwordDigest is valid \n' +
        '       | - user.wrongAttempts is 0  \n' +
        '       | - user.status is A(ctive)  \n' +
        '       | - user has multiple profiles  \n' +
        '       | - maxWrongAttempts is 3  \n' +
        '       | Expected :  \n' +
        '       | -> should return rspCode A(ccepted).  \n' +
        '', async () => {

            // 1. prepare test-data 

            // 1.1 create active user

            let password = chance.string({ length: 10 });

            let user = new User();
            user.code = chance.string({ length: 10 });
            user.password = await PasswordUtils.hashPassword(password, user.code);
            user.wrongAttempts = 0;
            user.status = "A";


            // 1.1.1 create profile
            let profile1 = new Profile();
            profile1.code = chance.string({ length: 10 });
            profile1 = await profileDao.create(trans, profile1, { user: "__super_user__" });

            let profile2 = new Profile();
            profile2.code = chance.string({length: 10});
            profile2 = await profileDao.create(trans, profile2, {user: "__super_user__"});

            // 1.1.2 create authz
            let authz = new Authz();
            authz.code = "LoginService";
            authz = await authzDao.create(trans, authz, { user: '__super_user__' });

            // 1.1.3 link between them
            profileAuthzDao.create(trans, { profile: profile1, authz, allowExecute: true }, { user: '__super_user__', include: ['profile', 'authz'] })

            // 1.1.4 create user
            user.profiles = [profile1, profile2];
            user = await userDao.create(trans, user, { include: ['profiles'], user: '__super_user__' });        

            // 1.2 preapre request data

            let req = {
                reqUser: user.code,
                reqRef: chance.string({ length: 10 }),
                passwordDigest: user.password
            }

            let options = {
                user: req.reqUser                
            };

            // 2. execute test

            let rsp = await loginservice.process(trans, req, options);

            // 3. compare outcome

            // 3.1. check the returned response            
            assert.ok(rsp);
            assert.strictEqual(rsp.rspCode, 'A');

            assert.strictEqual(rsp.user.code, req.reqUser);
            assert.strictEqual(rsp.user.wrongAttempts, 0);
            assert.strictEqual(rsp.user.status, 'A');

            // 3.2. check the user's wrongAttempts and status

            let userFindResult = (await userDao.findByFilter(trans, { code: req.reqUser }, { user: "__super_user__" }))[0];
            
            assert.strictEqual(userFindResult.code, req.reqUser);
            assert.strictEqual(userFindResult.wrongAttempts, 0);
            assert.strictEqual(userFindResult.status, 'A');

            // 3. check that request was logged

            let logFindResult = await loginLogDao.findByFilter(trans, { reqRef: req.reqRef }, { user: "__super_user__" });

            assert.ok(logFindResult);
            assert.strictEqual(logFindResult.length, 1);
            assert.strictEqual(logFindResult[0].rspCode, 'A');

            assert.ok(logFindResult[0].password !== password);
            assert.strictEqual(logFindResult[0].password.length, 60);

        });


    it('\n' +
        '       | Conditions: \n' +
        '       | - userCode provided doesn\'t exist \n' +
        '       | - passwordDigest is valid \n' +
        '       | - user.wrongAttempts is 0  \n' +
        '       | - user.status is A(ctive)  \n' +        
        '       | - maxWrongAttempts is 3  \n' +
        '       | Expected :  \n' +
        '       | -> should return rspCode D(eclined).  \n' +
        '       | -> rsp.rspReason should be ER_ACCESS_SRV__LOGIN_SRV__INV_ACCESS  \n' +
        '', async () => {

            // 1. prepare test-data 

            // 1.1 create active user

            let password = chance.string({ length: 10 });

            let user = new User();
            user.code = chance.string({ length: 10 });
            user.password = await PasswordUtils.hashPassword(password, user.code);
            user.wrongAttempts = 1;
            user.status = "A";


            // 1.1.1 create profile
            let profile = new Profile();
            profile.code = chance.string({ length: 10 });
            profile = await profileDao.create(trans, profile, { user: "__super_user__" });

            // 1.1.2 create authz
            let authz = new Authz();
            authz.code = "LoginService";
            authz = await authzDao.create(trans, authz, { user: '__super_user__' });

            // 1.1.3 link between them
            profileAuthzDao.create(trans, { profile, authz, allowExecute: true }, { user: '__super_user__', include: ['profile', 'authz'] })

            // 1.1.4 create user
            user.profiles = [profile];
            user = await userDao.create(trans, user, { include: ['profiles'], user: '__super_user__' });

            // 1.2 preapre request data

            let req = {
                reqUser: chance.string({ length: 10 }),
                reqRef: chance.string({ length: 10 }),
                passwordDigest: user.password
            }

            let options = {
                user: req.reqUser                
            };

            // 2. execute test

            let rsp = await loginservice.process(trans, req, options);

            // 3. compare outcome

            // 3.1. check the returned response        
            assert.ok(rsp);
            assert.strictEqual(rsp.rspCode, 'D');
            
            assert.strictEqual(rsp.reqRef, req.reqRef);
            assert.strictEqual(rsp.rspReason, "ER_ACCESS_SRV__LOGIN_SRV__INV_ACCESS");
            assert.strictEqual(rsp.rspMessage, 'Invalid username or password.');
            
         
            // 3.2 check that request was logged

            let logFindResult = await loginLogDao.findByFilter(trans, { reqRef: req.reqRef }, { user: "__super_user__" });

            assert.ok(logFindResult);
            assert.strictEqual(logFindResult.length, 1);
            assert.strictEqual(logFindResult[0].rspCode, 'D');
            assert.strictEqual(logFindResult[0].rspReason, 'ER_ACCESS_SRV__LOGIN_SRV__INV_ACCESS');
            assert.strictEqual(logFindResult[0].rspMessage, 'Invalid username or password.');

            assert.ok(logFindResult[0].password !== password);
            assert.strictEqual(logFindResult[0].password.length, 60);

        });


    it('\n' +
        '       | Conditions: \n' +
        '       | - userCode is valid \n' +
        '       | - password is invalid \n' +
        '       | - user.wrongAttempts is 0  \n' +
        '       | - user.status is A(ctive)  \n' +
        '       | - maxWrongAttempts is 3  \n' +
        '       | Expected :  \n' +
        '       | -> should return rspCode D(eclined).  \n' +
        '       | -> rsp.rspReason should be ER_ACCESS_SRV__LOGIN_SRV__INV_ACCESS \n' +
        '', async () => {

        // 1. prepare test-data 

        // 1.1 create active user

        let password = chance.string({ length: 10 });

        let user = new User();
        user.code = chance.string({ length: 10 });
        user.password = await PasswordUtils.hashPassword(password, user.code);
        user.wrongAttempts = 0;
        user.status = "A";


        // 1.1.1 create profile
        let profile = new Profile();
        profile.code = chance.string({ length: 10 });    
        profile = await profileDao.create(trans, profile, {user: "__super_user__"});

        // 1.1.2 create authz
        let authz = new Authz();
        authz.code = "LoginService";
        authz = await authzDao.create(trans, authz, { user: '__super_user__' });

        // 1.1.3 link between them
        profileAuthzDao.create(trans, {profile, authz, allowExecute: true}, {user: '__super_user__', include: ['profile', 'authz']})

        // 1.1.4 create user
        user.profiles = [profile];
        user = await userDao.create(trans, user, { include: ['profiles'], user: '__super_user__' });

        // 1.2 preapre request data

        let req = {
            reqUser: user.code,
            reqRef: chance.string({ length: 10 }),
            password: chance.string({ length: 10 })
        }

        let options = {
            user: req.reqUser            
        };

        // 2. execute test

        let rsp = await loginservice.process(trans, req, options);

        // 3. compare outcome

        // 3.1. check the returned response        
        assert.ok(rsp);
        assert.strictEqual(rsp.rspCode, 'D');

        assert.strictEqual(rsp.reqRef, req.reqRef);
        assert.strictEqual(rsp.rspReason, "ER_ACCESS_SRV__LOGIN_SRV__INV_ACCESS");
        assert.strictEqual(rsp.rspMessage, 'Invalid username or password.');

        // 3.2 check user wrong attempt
        let userFindResult = (await userDao.findByFilter(trans, { code: req.reqUser }, { user: "__super_user__" }))[0];

        assert.ok(userFindResult);
        assert.strictEqual(userFindResult.code, req.reqUser);
        assert.strictEqual(userFindResult.wrongAttempts, 1);
        assert.strictEqual(userFindResult.status, 'A');

        // 3.3 check that request was logged

        let logFindResult = await loginLogDao.findByFilter(trans, { reqRef: req.reqRef }, { user: "__super_user__" });

        assert.ok(logFindResult);
        assert.strictEqual(logFindResult.length, 1);
        assert.strictEqual(logFindResult[0].rspCode, 'D');
        assert.strictEqual(logFindResult[0].rspReason, 'ER_ACCESS_SRV__LOGIN_SRV__INV_ACCESS');
        assert.strictEqual(logFindResult[0].rspMessage, 'Invalid username or password.');

        assert.ok(logFindResult[0].password !== password);
        assert.strictEqual(logFindResult[0].password.length, 60);

    });


    it('\n' +
        '       | Conditions: \n' +
        '       | - userCode is valid \n' +
        '       | - password is valid \n' +
        '       | - user.wrongAttempts is 0  \n' +
        '       | - user.status is I(nactive)  \n' +        
        '       | - maxWrongAttempts is 3  \n' +
        '       | Expected :  \n' +
        '       | -> should return rspCode D(eclined).  \n' +
        '       | -> rsp.rspReason should be ER_ACCESS_SRV__LOGIN_SRV__USER_INACTIVE \n' +
        '', async () => {

        // 1. prepare test-data 

        // 1.1 create active user

        let password = chance.string({ length: 10 });

        let user = new User();
        user.code = chance.string({ length: 10 });
        user.password = await PasswordUtils.hashPassword(password, user.code);
        user.wrongAttempts = 0;
        user.status = "I";


        // 1.1.1 create profile
        let profile = new Profile();
        profile.code = chance.string({ length: 10 });    
        profile = await profileDao.create(trans, profile, {user: "__super_user__"});

        // 1.1.2 create authz
        let authz = new Authz();
        authz.code = "LoginService";
        authz = await authzDao.create(trans, authz, { user: '__super_user__' });

        // 1.1.3 link between them
        profileAuthzDao.create(trans, {profile, authz, allowExecute: true}, {user: '__super_user__', include: ['profile', 'authz']})

        // 1.1.4 create user
        user.profiles = [profile];
        user = await userDao.create(trans, user, { include: ['profiles'], user: '__super_user__' });

        // 1.2 preapre request data

        let req = {
            reqUser: user.code,
            reqRef: chance.string({ length: 10 }),
            password: password
        }

        let options = {
            user: req.reqUser
        };

        // 2. execute test

        let rsp = await loginservice.process(trans, req, options);

        // 3. compare outcome

        // 3.1. check the returned response        
        assert.ok(rsp);
        assert.strictEqual(rsp.rspCode, 'D');

        assert.strictEqual(rsp.reqRef, req.reqRef);
        assert.strictEqual(rsp.rspReason, "ER_ACCESS_SRV__LOGIN_SRV__USER_INACTIVE");
        assert.strictEqual(rsp.rspMessage, `The user ${user.code} is inactive.`);


        // 3.2 check that request was logged

        let logFindResult = await loginLogDao.findByFilter(trans, { reqRef: req.reqRef }, { user: "__super_user__" });

        assert.ok(logFindResult);
        assert.strictEqual(logFindResult.length, 1);
        assert.strictEqual(logFindResult[0].rspCode, 'D');
        assert.strictEqual(logFindResult[0].rspReason, 'ER_ACCESS_SRV__LOGIN_SRV__USER_INACTIVE');
        assert.strictEqual(logFindResult[0].rspMessage, `The user ${user.code} is inactive.`);

        assert.ok(logFindResult[0].password !== password);
        assert.strictEqual(logFindResult[0].password.length, 60);

    });



    it('\n' +
        '       | Conditions: \n' +
        '       | - userCode is valid \n' +
        '       | - password is valid \n' +
        '       | - user.wrongAttempts is 0  \n' +
        '       | - user.status is L(ocked)  \n' +
        '       | - maxWrongAttempts is 3  \n' +
        '       | Expected :  \n' +
        '       | -> should return rspCode D(eclined).  \n' +
        '       | -> rsp.rspReason should be ER_ACCESS_SRV__LOGIN_SRV__USER_LOCKED \n' +
        '', async () => {

        // 1. prepare test-data 

        // 1.1 create active user

        let password = chance.string({ length: 10 });

        let user = new User();
        user.code = chance.string({ length: 10 });
        user.password = await PasswordUtils.hashPassword(password, user.code);
        user.wrongAttempts = 1;
        user.status = "L";


        // 1.1.1 create profile
        let profile = new Profile();
        profile.code = chance.string({ length: 10 });    
        profile = await profileDao.create(trans, profile, {user: "__super_user__"});

        // 1.1.2 create authz
        let authz = new Authz();
        authz.code = "LoginService";
        authz = await authzDao.create(trans, authz, { user: '__super_user__' });

        // 1.1.3 link between them
        profileAuthzDao.create(trans, {profile, authz, allowExecute: true}, {user: '__super_user__', include: ['profile', 'authz']})

        // 1.1.4 create user
        user.profiles = [profile];
        user = await userDao.create(trans, user, { include: ['profiles'], user: '__super_user__' });

        // 1.2 preapre request data

        let req = {
            reqUser: user.code,
            reqRef: chance.string({ length: 10 }),
            password: password
        }

        let options = {
            user: req.reqUser            
        };

        // 2. execute test

        let rsp = await loginservice.process(trans, req, options);

        // 3. compare outcome

        // 3.1. check the returned response        
        assert.ok(rsp);
        assert.strictEqual(rsp.rspCode, 'D');

        assert.strictEqual(rsp.reqRef, req.reqRef);
        assert.strictEqual(rsp.rspReason, "ER_ACCESS_SRV__LOGIN_SRV__USER_LOCKED");
        assert.strictEqual(rsp.rspMessage, `The user ${user.code} is locked.`);


        // 3.2 check that request was logged

        let logFindResult = await loginLogDao.findByFilter(trans, { reqRef: req.reqRef }, { user: "__super_user__" });

        assert.ok(logFindResult);
        assert.strictEqual(logFindResult.length, 1);
        assert.strictEqual(logFindResult[0].rspCode, 'D');
        assert.strictEqual(logFindResult[0].rspReason, 'ER_ACCESS_SRV__LOGIN_SRV__USER_LOCKED');
        assert.strictEqual(logFindResult[0].rspMessage, `The user ${user.code} is locked.`);

        assert.ok(logFindResult[0].password !== password);
        assert.strictEqual(logFindResult[0].password.length, 60);

    });


    it('\n' +
        '       | Conditions: \n' +
        '       | - userCode is valid \n' +
        '       | - password is valid \n' +
        '       | - user.wrongAttempts is 10  \n' +
        '       | - user.status is A(ctive)  \n' +
        '       | - maxWrongAttempts is 3  \n' +
        '       | Expected :  \n' +
        '       | -> should return rspCode D(eclined).  \n' +
        '       | -> rsp.rspReason should be ER_ACCESS_SRV__LOGIN_SRV__WRONG_ATTEMPS_EXCEEDED \n' +
        '', async () => {

        // 1. prepare test-data 

        // 1.1 create active user

        let password = chance.string({ length: 10 });

        let user = new User();
        user.code = chance.string({ length: 10 });
        user.password = await PasswordUtils.hashPassword(password, user.code);
        user.wrongAttempts = 10;
        user.status = "A";


        // 1.1.1 create profile
        let profile = new Profile();
        profile.code = chance.string({ length: 10 });    
        profile = await profileDao.create(trans, profile, {user: "__super_user__"});

        // 1.1.2 create authz
        let authz = new Authz();
        authz.code = "LoginService";
        authz = await authzDao.create(trans, authz, { user: '__super_user__' });

        // 1.1.3 link between them
        profileAuthzDao.create(trans, {profile, authz, allowExecute: true}, {user: '__super_user__', include: ['profile', 'authz']})

        // 1.1.4 create user
        user.profiles = [profile];
        user = await userDao.create(trans, user, { include: ['profiles'], user: '__super_user__' });

        // 1.2 preapre request data

        let req = {
            reqUser: user.code,
            reqRef: chance.string({ length: 10 }),
            password: password
        }

        let options = {
            user: req.reqUser           
        };

        // 2. execute test

        let rsp = await loginservice.process(trans, req, options);

        // 3. compare outcome

        // 3.1. check the returned response        
        assert.ok(rsp);
        assert.strictEqual(rsp.rspCode, 'D');

        assert.strictEqual(rsp.reqRef, req.reqRef);
        assert.strictEqual(rsp.rspReason, "ER_ACCESS_SRV__LOGIN_SRV__WRONG_ATTEMPS_EXCEEDED");
        assert.strictEqual(rsp.rspMessage, `The user ${user.code} is inactive.`);


        // 3.2 check that request was logged

        let logFindResult = await loginLogDao.findByFilter(trans, { reqRef: req.reqRef }, { user: "__super_user__" });

        assert.ok(logFindResult);
        assert.strictEqual(logFindResult.length, 1);
        assert.strictEqual(logFindResult[0].rspCode, 'D');
        assert.strictEqual(logFindResult[0].rspReason, 'ER_ACCESS_SRV__LOGIN_SRV__WRONG_ATTEMPS_EXCEEDED');
        assert.strictEqual(logFindResult[0].rspMessage, `The user ${user.code} is inactive.`);

        assert.ok(logFindResult[0].password !== password);
        assert.strictEqual(logFindResult[0].password.length, 60);

    });

    it('\n' +
        '       | Conditions: \n' +
        '       | - userCode is valid \n' +
        '       | - password is invalid \n' +
        '       | - user.wrongAttempts is 2  \n' +
        '       | - user.status is A(ctive)  \n' +
        '       | - maxWrongAttempts is 3  \n' +
        '       | Expected :  \n' +
        '       | -> should return rspCode D(eclined).  \n' +
        '       | -> should lock user  \n' +
        '       | -> rsp.rspReason should be ER_ACCESS_SRV__LOGIN_SRV__INV_ACCESS \n' +
        '', async () => {

        // 1. prepare test-data 

        // 1.1 create active user

        let password = chance.string({ length: 10 });

        let user = new User();
        user.code = chance.string({ length: 10 });
        user.password = await PasswordUtils.hashPassword(password, user.code);
        user.wrongAttempts = 2;
        user.status = "A";


       // 1.1.1 create profile
        let profile = new Profile();
        profile.code = chance.string({ length: 10 });    
        profile = await profileDao.create(trans, profile, {user: "__super_user__"});

        // 1.1.2 create authz
        let authz = new Authz();
        authz.code = "LoginService";
        authz = await authzDao.create(trans, authz, { user: '__super_user__' });

        // 1.1.3 link between them
        profileAuthzDao.create(trans, {profile, authz, allowExecute: true}, {user: '__super_user__', include: ['profile', 'authz']})

        // 1.1.4 create user
        user.profiles = [profile];
        user = await userDao.create(trans, user, { include: ['profiles'], user: '__super_user__' });

        // 1.2 preapre request data

        let req = {
            reqUser: user.code,
            reqRef: chance.string({ length: 10 }),
            password: chance.string({ length: 10 })
        }

        let options = {
            user: req.reqUser
        };

        // 2. execute test

        let rsp = await loginservice.process(trans, req, options);

        // 3. compare outcome

        // 3.1. check the returned response        
        assert.ok(rsp);
        assert.strictEqual(rsp.rspCode, 'D');

        assert.strictEqual(rsp.reqRef, req.reqRef);
        assert.strictEqual(rsp.rspReason, "ER_ACCESS_SRV__LOGIN_SRV__INV_ACCESS");
        assert.strictEqual(rsp.rspMessage, 'Invalid username or password.');

        // 3.2 check user wrong attempt
        let userFindResult = (await userDao.findByFilter(trans, { code: req.reqUser }, { user: "__super_user__" }))[0];

        assert.ok(userFindResult);
        assert.strictEqual(userFindResult.code, req.reqUser);
        assert.strictEqual(userFindResult.wrongAttempts, 3);
        assert.strictEqual(userFindResult.status, 'L');

        // 3.3 check that request was logged

        let logFindResult = await loginLogDao.findByFilter(trans, { reqRef: req.reqRef }, { user: "__super_user__" });

        assert.ok(logFindResult);
        assert.strictEqual(logFindResult.length, 1);
        assert.strictEqual(logFindResult[0].rspCode, 'D');
        assert.strictEqual(logFindResult[0].rspReason, 'ER_ACCESS_SRV__LOGIN_SRV__INV_ACCESS');
        assert.strictEqual(logFindResult[0].rspMessage, 'Invalid username or password.');

        assert.ok(logFindResult[0].password !== password);
        assert.strictEqual(logFindResult[0].password.length, 60);

    });


});
