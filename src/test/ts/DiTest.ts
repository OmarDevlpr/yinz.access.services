import * as assert from "assert";
import * as stringify from "json-stringify-safe";
import Container from "@yinz/container/ts/Container";


const container = new Container({
    paramsFile: 'params.yml',
    modules: [
        '@yinz/commons/ts',
        '@yinz/commons.data/ts',
        '@yinz/access.data/ts',
        '@yinz/commons.services/ts',        
        process.cwd() + '/dist/main/ts'
    ],
});

const Exception = container.getClazz('Exception');


describe('| access.data.di', function () {
    it('| should assemble the module', function () {
        try {
            // classes
            assert.ok(container.getClazz('LoginService'));
            assert.ok(container.getClazz('SetupAccessService'));
            assert.ok(container.getClazz('ResetAccessService'));
            assert.ok(container.getClazz('ChangePasswordService'));
            assert.ok(container.getClazz('PrepareSetupAccessService'));
            assert.ok(container.getClazz('PrepareResetAccessService'));
            

            // beans
            assert.ok(container.getBean('loginService'));            
            assert.ok(container.getBean('setupAccessService'));            
            assert.ok(container.getBean('resetAccessService'));            
            assert.ok(container.getBean('prepareResetAccessService'));            
            assert.ok(container.getBean('changePasswordService'));            

        } catch (e) {
            if (e instanceof Exception) {
                assert.ok(false, 'exception: ' + e.reason + ': ' + stringify(e.extra));
            }
            else {
                assert.ok(false, 'exception: ' + e.message + '\n' + e.stack);
            }
        }
    });
});
