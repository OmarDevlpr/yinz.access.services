// libs
import * as assert from "assert";
import { Chance } from "chance";
import Container from "@yinz/container/ts/Container";
import { TypeormDao, TypeormDataSource } from "@yinz/commons.data";
import { YinzConnection, YinzTransaction } from "@yinz/commons.data/ts/DataSource";
import Authz from "@yinz/access.data/models/Authz";
import Profile from "@yinz/access.data/models/Profile";
import User from "@yinz/access.data/models/User";
import { PasswordUtils } from "@yinz/commons";
import SetupAccessService from "main/ts/SetupAccessService";
import ArchToken from "@yinz/token.data/models/ArchToken";
import TokenManager from "@yinz/token.services/ts/TokenManager";
import SetupAccessLog from "@yinz/access.data/models/SetupAccessLog";
import SetupAccess from "@yinz/access.data/models/SetupAccess";
import ProfileAuthz from "@yinz/access.data/models/ProfileAuthz";


const container = new Container({
    paramsFile: 'params.yml',
    modules: [
        '@yinz/commons/ts',
        '@yinz/commons.data/ts',
        '@yinz/access.data/ts',
        '@yinz/commons.services/ts',
        '@yinz/token.data/ts',
        '@yinz/token.services/ts',
        process.cwd() + '/dist/main/ts'
    ],
});

const chance = new Chance();
// const Exception = container.getClazz('Exception');

const dataSource = container.getBean<TypeormDataSource>('typeormDataSource');


const authzDao = container.getBean<TypeormDao<Authz>>('authzDao');
const profileAuthzDao = container.getBean<TypeormDao<ProfileAuthz>>('profileAuthzDao');
const userDao = container.getBean<TypeormDao<User>>('userDao');
const profileDao = container.getBean<TypeormDao<Profile>>('profileDao');
const setupAccessLogDao = container.getBean<TypeormDao<SetupAccessLog>>('setupAccessLogDao');
const setupAccessDao = container.getBean<TypeormDao<SetupAccess>>('setupAccessDao');
const archTokenDao = container.getBean<TypeormDao<ArchToken>>('archTokenDao');

const tokenManager = container.getBean<TokenManager>("tokenManager");

const setupAccessService = container.getBean<SetupAccessService>("setupAccessService");


let conn: YinzConnection;
let trans: YinzTransaction;



describe('| access.services <SetupAccessService>', function () {

    before(() => {
        return new Promise(async (resolve) => {
            conn = await dataSource.getConn();
            resolve(conn);
        })
    });

    after(() => {
        return new Promise(async (resolve) => {
            await dataSource.relConn(conn)
            resolve(true);
        })
    });

    beforeEach(() => {
        return new Promise(async (resolve) => {
            trans = await dataSource.startTrans(conn);
            resolve(trans);
        })
    });

    afterEach(() => {
        return new Promise(async (resolve) => {
            await dataSource.rollbackTrans(trans)
            resolve(true);
        })
    });

    it('\n' +
        '       | Conditions: \n' +
        '       | - token is valid \n' +
        '       | - password is valid \n' +
        '       | - token.data.setupMode is Automatic  \n' +
        '       | - token.data.profileIds is undefined \n' +                
        '       | Expected :  \n' +
        '       | -> should return rspCode A(ccepted).  \n' +
        '       | -> should create a user  \n' +
        '       | -> should archive the token  \n' +
        '', async () => {

            // 1. prepare test data
                        
            let userCode = chance.string({length: 10});
            let password = chance.string({length: 10});
            let tokenData = { setupMode: "Automatic"}

            let options = { user: "__super_user__" };

            let token = await tokenManager.generateToken(trans, userCode, tokenData, options)


            let req = {
                reqUser: "__anonymous__",
                tokenCode:  token.code,
                password: password,
                reqRef: chance.string({length: 10})
            }

            // 2. execute test

            let rsp = await setupAccessService.process(trans, req , options);

            // 3. compare result
            
            // 3.1 check rsp
            assert.ok(rsp)
            assert.strictEqual(rsp.rspCode, "A");

            // 3.2 check that user was created

            let userFindResult = await userDao.findByFilter(trans, { code: userCode}, options);

            assert.ok(userFindResult)
            assert.ok(userFindResult[0])
            assert.deepStrictEqual(userFindResult.length, 1)
            assert.deepStrictEqual(userFindResult[0].code, userCode)
            assert.deepStrictEqual(userFindResult[0].status, "A")
            assert.deepStrictEqual(userFindResult[0].password, await PasswordUtils.hashPassword(password, userCode));

            // 3.3 check archived token

            let archTokenFindResult = await archTokenDao.findByFilter(trans, {code: token.code}, options);

            assert.ok(archTokenFindResult)
            assert.ok(archTokenFindResult[0])
            assert.ok(archTokenFindResult[0].archOn)
            assert.deepStrictEqual(archTokenFindResult.length, 1)
            assert.deepStrictEqual(archTokenFindResult[0].code, token.code)          

            // 3.4 check that the request was logged              

            let setupAccessLogFindResult = await setupAccessLogDao.findByFilter(trans, {reqRef: req.reqRef}, options )

            assert.ok(setupAccessLogFindResult)
            assert.ok(setupAccessLogFindResult[0])            
            assert.deepStrictEqual(setupAccessLogFindResult[0].tokenCode, req.tokenCode)
            assert.deepStrictEqual(setupAccessLogFindResult[0].rspCode, "A")            

        });


    it('\n' +
        '       | Conditions: \n' +
        '       | - token is valid \n' +
        '       | - passwordDigest is valid \n' +
        '       | - token.data.setupMode is Automatic  \n' +
        '       | - token.data.profileIds is undefined \n' +
        '       | Expected :  \n' +
        '       | -> should return rspCode A(ccepted).  \n' +
        '       | -> should create a user  \n' +
        '       | -> should archive the token  \n' +
        '', async () => {

            // 1. prepare test data

            let userCode = chance.string({ length: 10 });
            let password = chance.string({ length: 10 });
            let tokenData = { setupMode: "Automatic" }

            let options = { user: "__super_user__" };

            let token = await tokenManager.generateToken(trans, userCode, tokenData, options)


            let req = {
                reqUser: "__anonymous__",
                tokenCode: token.code,
                passwordDigest: await PasswordUtils.hashPassword(password, userCode),
                reqRef: chance.string({ length: 10 })
            }

            // 2. execute test

            let rsp = await setupAccessService.process(trans, req, options);

            // 3. compare result

            // 3.1 check rsp
            assert.ok(rsp)
            assert.strictEqual(rsp.rspCode, "A");

            // 3.2 check that user was created

            let userFindResult = await userDao.findByFilter(trans, { code: userCode }, options);

            assert.ok(userFindResult)
            assert.ok(userFindResult[0])
            assert.deepStrictEqual(userFindResult.length, 1)
            assert.deepStrictEqual(userFindResult[0].code, userCode)
            assert.deepStrictEqual(userFindResult[0].status, "A")
            assert.deepStrictEqual(userFindResult[0].password, await PasswordUtils.hashPassword(password, userCode));

            // 3.3 check archived token

            let archTokenFindResult = await archTokenDao.findByFilter(trans, { code: token.code }, options);

            assert.ok(archTokenFindResult)
            assert.ok(archTokenFindResult[0])
            assert.ok(archTokenFindResult[0].archOn)
            assert.deepStrictEqual(archTokenFindResult.length, 1)
            assert.deepStrictEqual(archTokenFindResult[0].code, token.code)

            // 3.4 check that the request was logged              

            let setupAccessLogFindResult = await setupAccessLogDao.findByFilter(trans, { reqRef: req.reqRef }, options)

            assert.ok(setupAccessLogFindResult)
            assert.ok(setupAccessLogFindResult[0])            
            assert.deepStrictEqual(setupAccessLogFindResult[0].tokenCode, req.tokenCode)
            assert.deepStrictEqual(setupAccessLogFindResult[0].rspCode, "A")

        });
    

    it('\n' +
        '       | Conditions: \n' +
        '       | - token is valid \n' +
        '       | - passwordDigest is valid \n' +
        '       | - token.data.setupMode is Manual  \n' +
        '       | - token.data.profileIds is undefined \n' +
        '       | Expected :  \n' +
        '       | -> should return rspCode A(ccepted).  \n' +
        '       | -> should create a setupAccess  \n' +
        '       | -> should archive the token  \n' +
        '', async () => {

            // 1. prepare test data

            let userCode = chance.string({ length: 10 });
            let password = chance.string({ length: 10 });
            let tokenData = { setupMode: "Manual" }

            let options = { user: "__super_user__" };

            let token = await tokenManager.generateToken(trans, userCode, tokenData, options)


            let req = {
                reqUser: "__anonymous__",
                tokenCode: token.code,
                passwordDigest: await PasswordUtils.hashPassword(password, userCode),
                reqRef: chance.string({ length: 10 })
            }

            // 2. execute test

            let rsp = await setupAccessService.process(trans, req, options);

            // 3. compare result

            // 3.1 check rsp
            assert.ok(rsp)
            assert.strictEqual(rsp.rspCode, "A");

            // 3.2 check that setupAccess was created

            let setupAccessFindResult = await setupAccessDao.findByFilter(trans, { code: userCode }, options);

            assert.ok(setupAccessFindResult)
            assert.ok(setupAccessFindResult[0])
            assert.deepStrictEqual(setupAccessFindResult.length, 1)
            assert.deepStrictEqual(setupAccessFindResult[0].code, userCode)
            assert.deepStrictEqual(setupAccessFindResult[0].status, "W")
            assert.deepStrictEqual(setupAccessFindResult[0].password, await PasswordUtils.hashPassword(password, userCode));

            // 3.3 check archived token

            let archTokenFindResult = await archTokenDao.findByFilter(trans, { code: token.code }, options);

            assert.ok(archTokenFindResult)
            assert.ok(archTokenFindResult[0])
            assert.ok(archTokenFindResult[0].archOn)
            assert.deepStrictEqual(archTokenFindResult.length, 1)
            assert.deepStrictEqual(archTokenFindResult[0].code, token.code)

            // 3.4 check that the request was logged              

            let setupAccessLogFindResult = await setupAccessLogDao.findByFilter(trans, { reqRef: req.reqRef }, options)

            assert.ok(setupAccessLogFindResult)
            assert.ok(setupAccessLogFindResult[0])            
            assert.deepStrictEqual(setupAccessLogFindResult[0].tokenCode, req.tokenCode)
            assert.deepStrictEqual(setupAccessLogFindResult[0].rspCode, "A")

        });



    it('\n' +
        '       | Conditions: \n' +
        '       | - token is valid \n' +
        '       | - passwordDigest is valid \n' +
        '       | - token.data.setupMode is X  \n' +
        '       | - token.data.profileIds is undefined \n' +
        '       | Expected :  \n' +
        '       | -> should return rspCode A(ccepted).  \n' +
        '       | -> should create a setupAccess  \n' +
        '       | -> should archive the token  \n' +
        '', async () => {

            // 1. prepare test data

            let userCode = chance.string({ length: 10 });
            let password = chance.string({ length: 10 });
            let tokenData = { setupMode: "X" }

            let options = { user: "__super_user__" };

            let token = await tokenManager.generateToken(trans, userCode, tokenData, options)


            let req = {
                reqUser: "__anonymous__",
                tokenCode: token.code,
                passwordDigest: await PasswordUtils.hashPassword(password, userCode),
                reqRef: chance.string({ length: 10 })
            }

            // 2. execute test

            let rsp = await setupAccessService.process(trans, req, options);

            // 3. compare result

            // 3.1 check rsp
            assert.ok(rsp)
            assert.strictEqual(rsp.rspCode, "A");

            // 3.2 check that setupAccess was created

            let setupAccessFindResult = await setupAccessDao.findByFilter(trans, { code: userCode }, options);

            assert.ok(setupAccessFindResult)
            assert.ok(setupAccessFindResult[0])
            assert.deepStrictEqual(setupAccessFindResult.length, 1)
            assert.deepStrictEqual(setupAccessFindResult[0].code, userCode)
            assert.deepStrictEqual(setupAccessFindResult[0].status, "W")
            assert.deepStrictEqual(setupAccessFindResult[0].password, await PasswordUtils.hashPassword(password, userCode));

            // 3.3 check archived token

            let archTokenFindResult = await archTokenDao.findByFilter(trans, { code: token.code }, options);

            assert.ok(archTokenFindResult)
            assert.ok(archTokenFindResult[0])
            assert.ok(archTokenFindResult[0].archOn)
            assert.deepStrictEqual(archTokenFindResult.length, 1)
            assert.deepStrictEqual(archTokenFindResult[0].code, token.code)

            // 3.4 check that the request was logged              

            let setupAccessLogFindResult = await setupAccessLogDao.findByFilter(trans, { reqRef: req.reqRef }, options)

            assert.ok(setupAccessLogFindResult)
            assert.ok(setupAccessLogFindResult[0])
            assert.deepStrictEqual(setupAccessLogFindResult[0].tokenCode, req.tokenCode)
            assert.deepStrictEqual(setupAccessLogFindResult[0].rspCode, "A")

        });


    it('\n' +
        '       | Conditions: \n' +
        '       | - token is valid \n' +
        '       | - passwordDigest is valid \n' +
        '       | - token.data.setupMode is undefined  \n' +
        '       | - token.data.profileIds is undefined \n' +
        '       | Expected :  \n' +
        '       | -> should return rspCode A(ccepted).  \n' +
        '       | -> should create a setupAccess  \n' +
        '       | -> should archive the token  \n' +
        '', async () => {

            // 1. prepare test data

            let userCode = chance.string({ length: 10 });
            let password = chance.string({ length: 10 });
            let tokenData = { setupMode: undefined }

            let options = { user: "__super_user__" };

            let token = await tokenManager.generateToken(trans, userCode, tokenData, options)


            let req = {
                reqUser: "__anonymous__",
                tokenCode: token.code,
                passwordDigest: await PasswordUtils.hashPassword(password, userCode),
                reqRef: chance.string({ length: 10 })
            }

            // 2. execute test

            let rsp = await setupAccessService.process(trans, req, options);

            // 3. compare result

            // 3.1 check rsp
            assert.ok(rsp)
            assert.strictEqual(rsp.rspCode, "A");

            // 3.2 check that setupAccess was created

            let setupAccessFindResult = await setupAccessDao.findByFilter(trans, { code: userCode }, options);

            assert.ok(setupAccessFindResult)
            assert.ok(setupAccessFindResult[0])
            assert.deepStrictEqual(setupAccessFindResult.length, 1)
            assert.deepStrictEqual(setupAccessFindResult[0].code, userCode)
            assert.deepStrictEqual(setupAccessFindResult[0].status, "W")
            assert.deepStrictEqual(setupAccessFindResult[0].password, await PasswordUtils.hashPassword(password, userCode));

            // 3.3 check archived token

            let archTokenFindResult = await archTokenDao.findByFilter(trans, { code: token.code }, options);

            assert.ok(archTokenFindResult)
            assert.ok(archTokenFindResult[0])
            assert.ok(archTokenFindResult[0].archOn)
            assert.deepStrictEqual(archTokenFindResult.length, 1)
            assert.deepStrictEqual(archTokenFindResult[0].code, token.code)

            // 3.4 check that the request was logged              

            let setupAccessLogFindResult = await setupAccessLogDao.findByFilter(trans, { reqRef: req.reqRef }, options)

            assert.ok(setupAccessLogFindResult)
            assert.ok(setupAccessLogFindResult[0])
            assert.deepStrictEqual(setupAccessLogFindResult[0].tokenCode, req.tokenCode)
            assert.deepStrictEqual(setupAccessLogFindResult[0].rspCode, "A")

        });

    it('\n' +
        '       | Conditions: \n' +
        '       | - token is valid \n' +
        '       | - password is valid \n' +
        '       | - token.data.setupMode is Automatic  \n' +
        '       | - token.data.profileIds is provided \n' +
        '       | Expected :  \n' +
        '       | -> should return rspCode A(ccepted).  \n' +
        '       | -> should create a user with the provided profiles  \n' +
        '       | -> should archive the token  \n' +
        '', async () => {

            // 1. prepare test data

            let userCode = chance.string({ length: 10 });
            let password = chance.string({ length: 10 });            
            let options = { user: "__super_user__" };
            
            // 1.1 create profiles 

            let authz = new Authz();
            authz.code = chance.string({ length: 10 });
            authz.name = chance.string({ length: 10 });
            authz = await authzDao.create(trans, authz, options);

            let profile = new Profile();
            profile.code = chance.string({ length: 10});
            profile.name = chance.string({ length: 10 });
            profile = await profileDao.create(trans, profile, { ...options });

            profileAuthzDao.create(trans, { profile, authz, allowExecute: true }, { user: '__super_user__', include: ['profile', 'authz'] })
            
            
            let tokenData = { setupMode: "Automatic", profileIds: [profile.id] }

            let token = await tokenManager.generateToken(trans, userCode, tokenData, options)


            let req = {
                reqUser: "__anonymous__",
                tokenCode: token.code,
                password: password,
                reqRef: chance.string({ length: 10 })
            }

            // 2. execute test

            let rsp = await setupAccessService.process(trans, req, options);            

            // 3. compare result

            // 3.1 check rsp
            assert.ok(rsp)
            assert.strictEqual(rsp.rspCode, "A");

            // 3.2 check that user was created

            let userFindResult = await userDao.findByFilter(trans, { code: userCode }, {...options, include: ['profiles']});

            assert.ok(userFindResult)
            assert.ok(userFindResult[0])
            assert.ok(userFindResult[0].profiles)            
            assert.deepStrictEqual(userFindResult.length, 1)

            assert.deepStrictEqual(userFindResult[0].profiles.length, 1)
            assert.deepStrictEqual(userFindResult[0].profiles[0].id, profile.id);
            assert.deepStrictEqual(userFindResult[0].profiles[0].code, profile.code);            

            assert.deepStrictEqual(userFindResult[0].code, userCode)
            assert.deepStrictEqual(userFindResult[0].status, "A")
            assert.deepStrictEqual(userFindResult[0].password, await PasswordUtils.hashPassword(password, userCode));
            


            // 3.3 check archived token

            let archTokenFindResult = await archTokenDao.findByFilter(trans, { code: token.code }, options);

            assert.ok(archTokenFindResult)
            assert.ok(archTokenFindResult[0])
            assert.ok(archTokenFindResult[0].archOn)
            assert.deepStrictEqual(archTokenFindResult.length, 1)
            assert.deepStrictEqual(archTokenFindResult[0].code, token.code)

            // 3.4 check that the request was logged              

            let setupAccessLogFindResult = await setupAccessLogDao.findByFilter(trans, { reqRef: req.reqRef }, options)

            assert.ok(setupAccessLogFindResult)
            assert.ok(setupAccessLogFindResult[0])            
            assert.deepStrictEqual(setupAccessLogFindResult[0].tokenCode, req.tokenCode)
            assert.deepStrictEqual(setupAccessLogFindResult[0].rspCode, "A")

        });



    it('\n' +
        '       | Conditions: \n' +
        '       | - token is valid \n' +
        '       | - passwordDigest is valid \n' +
        '       | - token.data.setupMode is Manual  \n' +
        '       | - token.data.profileIds is provided \n' +
        '       | Expected :  \n' +
        '       | -> should return rspCode A(ccepted).  \n' +
        '       | -> should create a setupAccess  \n' +
        '       | -> should archive the token  \n' +
        '', async () => {

            // 1. prepare test data

            let userCode = chance.string({ length: 10 });
            let password = chance.string({ length: 10 });
            let options = { user: "__super_user__" };

            // 1.1 create profiles 

            let authz = new Authz();
            authz.code = chance.string({ length: 10 });
            authz.name = chance.string({ length: 10 });
            authz = await authzDao.create(trans, authz, options);

            let profile = new Profile();
            profile.code = chance.string({ length: 10 });
            profile.name = chance.string({ length: 10 });
            profile = await profileDao.create(trans, profile, { ...options });

            profileAuthzDao.create(trans, { profile, authz, allowExecute: true }, { user: '__super_user__', include: ['profile', 'authz'] })

            let tokenData = { setupMode: "Manual", profileIds: [profile.id] }

            let token = await tokenManager.generateToken(trans, userCode, tokenData, options)


            let req = {
                reqUser: "__anonymous__",
                tokenCode: token.code,
                password: password,
                reqRef: chance.string({ length: 10 })
            }

            // 2. execute test

            let rsp = await setupAccessService.process(trans, req, options);

            // 3. compare result

            // 3.1 check rsp
            assert.ok(rsp)
            assert.strictEqual(rsp.rspCode, "A");

            // 3.2 check that setupAccess was created

            let setupAccessFindResult = await setupAccessDao.findByFilter(trans, { code: userCode }, {...options, include: ["profiles"]});

            assert.ok(setupAccessFindResult)
            assert.ok(setupAccessFindResult[0])
            assert.deepStrictEqual(setupAccessFindResult.length, 1)


            assert.deepStrictEqual(setupAccessFindResult[0].profiles.length, 1)
            assert.deepStrictEqual(setupAccessFindResult[0].profiles[0].id, profile.id);
            assert.deepStrictEqual(setupAccessFindResult[0].profiles[0].code, profile.code);      

            assert.deepStrictEqual(setupAccessFindResult[0].code, userCode)
            assert.deepStrictEqual(setupAccessFindResult[0].status, "W")
            assert.deepStrictEqual(setupAccessFindResult[0].password, await PasswordUtils.hashPassword(password, userCode));

            // 3.3 check archived token

            let archTokenFindResult = await archTokenDao.findByFilter(trans, { code: token.code }, options);

            assert.ok(archTokenFindResult)
            assert.ok(archTokenFindResult[0])
            assert.ok(archTokenFindResult[0].archOn)
            assert.deepStrictEqual(archTokenFindResult.length, 1)
            assert.deepStrictEqual(archTokenFindResult[0].code, token.code)

            // 3.4 check that the request was logged              

            let setupAccessLogFindResult = await setupAccessLogDao.findByFilter(trans, { reqRef: req.reqRef }, options)

            assert.ok(setupAccessLogFindResult)
            assert.ok(setupAccessLogFindResult[0])            
            assert.deepStrictEqual(setupAccessLogFindResult[0].tokenCode, req.tokenCode)
            assert.deepStrictEqual(setupAccessLogFindResult[0].rspCode, "A")

        });

    it('\n' +
        '       | Conditions: \n' +
        '       | - token is not a string \n' +
        '       | - password is valid \n' +
        '       | - token.data.setupMode is Automatic  \n' +
        '       | - token.data.profileIds is undefined \n' +
        '       | Expected :  \n' +
        '       | -> should return rspCode D(eclined).  \n' +        
        '       | -> rsp.rspReason should be ERR_ACCESS_SRV__SETUP_ACCESS_SRV__INV_TOKEN \n' +        
        '', async () => {

            // 1. prepare test data
            
            let password = chance.string({ length: 10 });
            
            let options = { user: "__super_user__" };            

            let req = {
                reqUser: "__anonymous__",
                tokenCode: "",
                password: password,
                reqRef: chance.string({ length: 10 })
            }

            // 2. execute test

            let rsp = await setupAccessService.process(trans, req, options);

            // 3. compare result

            // 3.1 check rsp
            assert.ok(rsp)
            assert.strictEqual(rsp.rspCode, "D");
            assert.strictEqual(rsp.rspReason, "ERR_ACCESS_SRV__SETUP_ACCESS_SRV__INV_TOKEN");
            
        });


    it('\n' +
        '       | Conditions: \n' +
        '       | - token is valid  \n' +
        '       | - password is invalid \n' +
        '       | - token.data.setupMode is Automatic  \n' +
        '       | - token.data.profileIds is undefined \n' +
        '       | Expected :  \n' +
        '       | -> should return rspCode D(eclined).  \n' +
        '       | -> rsp.rspReason should be ERR_ACCESS_SRV__SETUP_ACCESS_SRV__INV_PWD \n' +
        '', async () => {

            // 1. prepare test data

            
            let options = { user: "__super_user__" };

            let req = {
                reqUser: "__anonymous__",
                tokenCode: chance.string({ length: 10 }),
                password: {} as any,
                reqRef: chance.string({ length: 10 })
            }

            // 2. execute test

            let rsp = await setupAccessService.process(trans, req, options);

            // 3. compare result

            // 3.1 check rsp
            assert.ok(rsp)
            assert.strictEqual(rsp.rspCode, "D");
            assert.strictEqual(rsp.rspReason, "ERR_ACCESS_SRV__SETUP_ACCESS_SRV__INV_PWD");

        });

});
