// libs
import * as assert from "assert";
import { Chance } from "chance";
import Container from "@yinz/container/ts/Container";
import { TypeormDao, TypeormDataSource } from "@yinz/commons.data";
import { YinzConnection, YinzTransaction } from "@yinz/commons.data/ts/DataSource";
// import Authz from "@yinz/access.data/models/Authz";
// import Profile from "@yinz/access.data/models/Profile";
// import User from "@yinz/access.data/models/User";
import PrepareSetupAccessLog from "@yinz/access.data/models/PrepareSetupAccessLog";
import { YinzOptions } from "@yinz/commons";
import PrepareSetupAccessService, { PrepareSetupAccessServiceReq, PrepareSetupAccessServiceRsp, AccessOwner } from "../../main/ts/PrepareSetupAccessService";
import Person from "@yinz/basic.data/models/Person";
import Token from "@yinz/token.data/models/Token";
import Notif from "@yinz/notif.data/models/Notif";
import SetupAccessService from "../../main/ts/SetupAccessService";


const container = new Container({
    paramsFile: 'params.yml',
    modules: [
        '@yinz/commons/ts',
        '@yinz/commons.data/ts',
        '@yinz/access.data/ts',
        '@yinz/commons.services/ts',
        '@yinz/token.data/ts',
        '@yinz/token.services/ts',
        '@yinz/notif.data/ts',        
        '@yinz/notif.services/ts',        
        process.cwd() + '/dist/main/ts'
    ],
});

const chance = new Chance();
// const Exception = container.getClazz('Exception');

const dataSource = container.getBean<TypeormDataSource>('typeormDataSource');



// const userDao = container.getBean<TypeormDao<User>>('userDao');

const notifDao = container.getBean<TypeormDao<Notif>>('notifDao');
const tokenDao = container.getBean<TypeormDao<Token>>('tokenDao');
const prepareSetupAccessLogDao = container.getBean<TypeormDao<PrepareSetupAccessLog>>('prepareSetupAccessLogDao');

const setupAccessService = container.getBean<SetupAccessService>("setupAccessService");

class PrepareSetupSomeAccessService extends PrepareSetupAccessService<any, any> {
    constructor(options) {
        super(options);

    }


    protected async createAccessOwner (handler: YinzConnection | YinzConnection, req: PrepareSetupAccessServiceReq,
        rsp: PrepareSetupAccessServiceRsp, options?: YinzOptions): Promise<AccessOwner> {
        return {
            owner: {
               id: 1
            } as Person,
            ownerModelName: 'Some',
        }
    }
}

let prepareSetupSomeAccessService = new PrepareSetupSomeAccessService({
    accessManager: container.getBean('accessManager'),
    logger: container.getBean('logger'),
    logDao: container.getBean('prepareSetupAccessLogDao'),
    logReqRsp: true,
    notifManager: container.getBean('notifManager'),
    notifMessageTempl: null,
    notifSubjectTempl: null,
    profileDao: container.getBean('profileDao'),
    tokenManager: container.getBean('tokenManager'),
    userDao: container.getBean('userDao'),
});

container.setBean('prepareSetupSomeAccessService', prepareSetupSomeAccessService);


let conn: YinzConnection;
let trans: YinzTransaction;



describe('| access.services <PrepareSetupAccessService>', function () {

    before(() => {
        return new Promise(async (resolve) => {
            conn = await dataSource.getConn();
            resolve(conn);
        })
    });

    after(() => {
        return new Promise(async (resolve) => {
            await dataSource.relConn(conn)
            resolve(true);
        })
    });

    beforeEach(() => {
        return new Promise(async (resolve) => {
            trans = await dataSource.startTrans(conn);
            resolve(trans);
        })
    });

    afterEach(() => {
        return new Promise(async (resolve) => {
            await dataSource.rollbackTrans(trans)
            resolve(true);
        })
    });
  

    it('\n' +
        '       | Conditions: \n' +
        '       | - email is valid \n' +
        '       | - firstName is valid \n' +        
        '       | Expected :  \n' +
        '       | -> should return rspCode A(ccepted).  \n' +
        '', async () => {

            // 1. prepare test-data 
            
            let req = {
                email: chance.string({length: 10}),
                firstName: chance.string({ length: 10 }),
                reqRef: chance.string({length: 10})
            }
            
            let options = { user: "__super_user__"}

            // 2. execute test

            let rsp = await prepareSetupSomeAccessService.process(trans, req, options);

            // 3. compare outcome

            // 3.1. check the returned response        
            assert.ok(rsp);
            assert.strictEqual(rsp.rspCode, 'A');            

            // 3.2. check that token was generated

            let tokenFindResult = await tokenDao.findAll(trans, { user: "__super_user__" });

            assert.ok(tokenFindResult);            
            assert.strictEqual(tokenFindResult.length, 1);
            assert.strictEqual(tokenFindResult[0].owner, req.email);
            assert.deepStrictEqual(JSON.parse(tokenFindResult[0].data), {setupMode: "Automatic", ownerId: 1, ownerModelName: "Some"});
            assert.strictEqual(tokenFindResult[0].maxNumberOfUse, 1);
            assert.strictEqual(tokenFindResult[0].numberOfUse, 0);

            // 3.3 check that notification was created

            let notifFindResult = await notifDao.findAll(trans, { user: "__super_user__" });
            
            assert.ok(notifFindResult);
            assert.strictEqual(notifFindResult.length, 1);
            assert.strictEqual(notifFindResult[0].dest, req.email);
            assert.strictEqual(notifFindResult[0].destType, 'E');
            assert.strictEqual(notifFindResult[0].reason, 'SETUP_ACCESS');
            assert.strictEqual(notifFindResult[0].subject, 'setupAccessInstructionsMessageSubject');
            assert.strictEqual(notifFindResult[0].message, 'setupAccessInstructionsMessageBody');

            // 3.4 check that the request has been logged
            let logFindResult = await prepareSetupAccessLogDao.findByFilter(trans, { reqRef: req.reqRef }, { user: "__super_user__" });

            assert.ok(logFindResult);
            assert.strictEqual(logFindResult.length, 1);
            assert.strictEqual(logFindResult[0].rspCode, 'A');
            
            assert.strictEqual(logFindResult[0].email, req.email);
            assert.strictEqual(logFindResult[0].firstName, req.firstName);
            assert.strictEqual(logFindResult[0].reqRef, req.reqRef);

        });  


    it('\n' +
        '       | Conditions: \n' +
        '       | - phone is valid \n' +
        '       | - firstName is valid \n' +
        '       | Expected :  \n' +
        '       | -> should return rspCode A(ccepted).  \n' +
        '', async () => {

            // 1. prepare test-data 

            let req = {
                phone: chance.string({ length: 10 }),
                firstName: chance.string({ length: 10 }),
                reqRef: chance.string({ length: 10 })
            }

            let options = { user: "__super_user__" }

            // 2. execute test

            let rsp = await prepareSetupSomeAccessService.process(trans, req, options);

            // 3. compare outcome

            // 3.1. check the returned response        
            assert.ok(rsp);
            assert.strictEqual(rsp.rspCode, 'A');

            // 3.2. check that token was generated

            let tokenFindResult = await tokenDao.findAll(trans, { user: "__super_user__" });

            assert.ok(tokenFindResult);
            assert.strictEqual(tokenFindResult.length, 1);
            assert.strictEqual(tokenFindResult[0].owner, req.phone);
            assert.deepStrictEqual(JSON.parse(tokenFindResult[0].data), { setupMode: "Automatic", ownerId: 1, ownerModelName: "Some" });
            assert.strictEqual(tokenFindResult[0].maxNumberOfUse, 1);
            assert.strictEqual(tokenFindResult[0].numberOfUse, 0);

            // 3.3 check that notification was created

            let notifFindResult = await notifDao.findAll(trans, { user: "__super_user__" });

            assert.ok(notifFindResult);
            assert.strictEqual(notifFindResult.length, 1);
            assert.strictEqual(notifFindResult[0].dest, req.phone);
            assert.strictEqual(notifFindResult[0].destType, 'S');
            assert.strictEqual(notifFindResult[0].reason, 'SETUP_ACCESS');
            assert.strictEqual(notifFindResult[0].subject, 'setupAccessInstructionsMessageSubject');
            assert.strictEqual(notifFindResult[0].message, 'setupAccessInstructionsMessageBody');

            // 3.4 check that the request has been logged
            let logFindResult = await prepareSetupAccessLogDao.findByFilter(trans, { reqRef: req.reqRef }, { user: "__super_user__" });

            assert.ok(logFindResult);
            assert.strictEqual(logFindResult.length, 1);
            assert.strictEqual(logFindResult[0].rspCode, 'A');

            assert.strictEqual(logFindResult[0].phone, req.phone);
            assert.strictEqual(logFindResult[0].firstName, req.firstName);
            assert.strictEqual(logFindResult[0].reqRef, req.reqRef);

        });  


    it('\n' +
        '       | Conditions: \n' +
        '       | - phone is valid \n' +
        '       | - firstName is valid \n' +
        '       | - model is valid \n' +
        '       | Expected :  \n' +
        '       | -> should add the model data to notif template.  \n' +
        '       | -> should return rspCode A(ccepted).  \n' +
        '', async () => {

            // 1. prepare test-data 

            let model = {
                name: chance.string({length: 10})
            }

            let req = {
                phone: chance.string({ length: 10 }),
                firstName: chance.string({ length: 10 }),
                reqRef: chance.string({ length: 10 })
            }

            let options = { user: "__super_user__" }

            // 2. execute test

            let rsp = await prepareSetupSomeAccessService.process(trans, req, {...options, model});

            // 3. compare outcome

            // 3.1. check the returned response        
            assert.ok(rsp);
            assert.strictEqual(rsp.rspCode, 'A');

            // 3.2. check that token was generated

            let tokenFindResult = await tokenDao.findAll(trans, { user: "__super_user__" });

            assert.ok(tokenFindResult);
            assert.strictEqual(tokenFindResult.length, 1);
            assert.strictEqual(tokenFindResult[0].owner, req.phone);
            assert.deepStrictEqual(JSON.parse(tokenFindResult[0].data), { setupMode: "Automatic", ownerId: 1, ownerModelName: "Some" });
            assert.strictEqual(tokenFindResult[0].maxNumberOfUse, 1);
            assert.strictEqual(tokenFindResult[0].numberOfUse, 0);

            // 3.3 check that notification was created

            let notifFindResult = await notifDao.findAll(trans, { user: "__super_user__" });

            assert.ok(notifFindResult);
            assert.strictEqual(notifFindResult.length, 1);
            assert.strictEqual(notifFindResult[0].dest, req.phone);
            assert.strictEqual(notifFindResult[0].destType, 'S');
            assert.strictEqual(notifFindResult[0].reason, 'SETUP_ACCESS');
            assert.strictEqual(notifFindResult[0].subject, 'setupAccessInstructionsMessageSubject');
            assert.strictEqual(notifFindResult[0].message, 'setupAccessInstructionsMessageBody');
            assert.strictEqual(notifFindResult[0].model.name, model.name);

            // 3.4 check that the request has been logged
            let logFindResult = await prepareSetupAccessLogDao.findByFilter(trans, { reqRef: req.reqRef }, { user: "__super_user__" });

            assert.ok(logFindResult);
            assert.strictEqual(logFindResult.length, 1);
            assert.strictEqual(logFindResult[0].rspCode, 'A');

            assert.strictEqual(logFindResult[0].phone, req.phone);
            assert.strictEqual(logFindResult[0].firstName, req.firstName);
            assert.strictEqual(logFindResult[0].reqRef, req.reqRef);

        });  

    
    it('\n' +
        '       | Conditions: \n' +
        '       | - nor email or phone is provided \n' +
        '       | - firstName is valid \n' +
        '       | Expected :  \n' +
        '       | -> should return rspCode D(eclined).  \n' +
        '', async () => {

            // 1. prepare test-data 

            let req = {                
                firstName: chance.string({ length: 10 }),
                reqRef: chance.string({ length: 10 })
            }

            let options = { user: "__super_user__" }

            // 2. execute test

            let rsp = await prepareSetupSomeAccessService.process(trans, req, options);

            // 3. compare outcome

            // 3.1. check the returned response        
            assert.ok(rsp);
            assert.strictEqual(rsp.rspCode, 'D');
            assert.strictEqual(rsp.rspReason, 'ERR_ACCESS_SRV__PREP_SETUP_ACCESS_SRV__MISS_EMAIL_AND_PHONE');
            assert.strictEqual(rsp.rspMessage, 'Both email and phone are missing in the request! You should supply one of the two and it should be a non-empty-string.');

            // 3.2. check that token was not generated

            let tokenFindResult = await tokenDao.findAll(trans, { user: "__super_user__" });

            assert.ok(tokenFindResult);
            assert.strictEqual(tokenFindResult.length, 0);
        
            // 3.3 check that notification was not created

            let notifFindResult = await notifDao.findAll(trans, { user: "__super_user__" });

            assert.ok(notifFindResult);
            assert.strictEqual(notifFindResult.length, 0);
            
            // 3.4 check that the request has been logged
            let logFindResult = await prepareSetupAccessLogDao.findByFilter(trans, { reqRef: req.reqRef }, { user: "__super_user__" });

            assert.ok(logFindResult);
            assert.strictEqual(logFindResult.length, 1);
            assert.strictEqual(logFindResult[0].rspCode, 'D');
            assert.strictEqual(logFindResult[0].rspReason, 'ERR_ACCESS_SRV__PREP_SETUP_ACCESS_SRV__MISS_EMAIL_AND_PHONE');            
            assert.strictEqual(logFindResult[0].firstName, req.firstName);
            assert.strictEqual(logFindResult[0].reqRef, req.reqRef);

        });  


    it('\n' +
        '       | Conditions: \n' +
        '       | - email is empty string \n' +
        '       | - firstName is valid \n' +
        '       | Expected :  \n' +
        '       | -> should return rspCode D(eclined).  \n' +
        '', async () => {

            // 1. prepare test-data 

            let req = {
                email: "",
                firstName: chance.string({ length: 10 }),
                reqRef: chance.string({ length: 10 })
            }

            let options = { user: "__super_user__" }

            // 2. execute test

            let rsp = await prepareSetupSomeAccessService.process(trans, req, options);

            // 3. compare outcome

            // 3.1. check the returned response        
            assert.ok(rsp);
            assert.strictEqual(rsp.rspCode, 'D');
            assert.strictEqual(rsp.rspReason, 'ERR_ACCESS_SRV__PREP_SETUP_ACCESS_SRV__INV_EMAIL');
            assert.strictEqual(rsp.rspMessage, 'The supplied email ' + null + ' is invalid! It should be a none-empty string.');

            // 3.2. check that token was not generated

            let tokenFindResult = await tokenDao.findAll(trans, { user: "__super_user__" });

            assert.ok(tokenFindResult);
            assert.strictEqual(tokenFindResult.length, 0);

            // 3.3 check that notification was not created

            let notifFindResult = await notifDao.findAll(trans, { user: "__super_user__" });

            assert.ok(notifFindResult);
            assert.strictEqual(notifFindResult.length, 0);

            // 3.4 check that the request has been logged
            let logFindResult = await prepareSetupAccessLogDao.findByFilter(trans, { reqRef: req.reqRef }, { user: "__super_user__" });

            assert.ok(logFindResult);
            assert.strictEqual(logFindResult.length, 1);
            assert.strictEqual(logFindResult[0].rspCode, 'D');
            assert.strictEqual(logFindResult[0].rspReason, 'ERR_ACCESS_SRV__PREP_SETUP_ACCESS_SRV__INV_EMAIL');
            assert.strictEqual(logFindResult[0].rspMessage, 'The supplied email ' + null + ' is invalid! It should be a none-empty string.');
            assert.strictEqual(logFindResult[0].firstName, req.firstName);
            assert.strictEqual(logFindResult[0].reqRef, req.reqRef);

        });  


    it('\n' +
        '       | Conditions: \n' +
        '       | - phone is empty string \n' +
        '       | - firstName is valid \n' +
        '       | Expected :  \n' +
        '       | -> should return rspCode D(eclined).  \n' +
        '', async () => {

            // 1. prepare test-data 

            let req = {
                phone: "",
                firstName: chance.string({ length: 10 }),
                reqRef: chance.string({ length: 10 })
            }

            let options = { user: "__super_user__" }

            // 2. execute test

            let rsp = await prepareSetupSomeAccessService.process(trans, req, options);

            // 3. compare outcome

            // 3.1. check the returned response        
            assert.ok(rsp);
            assert.strictEqual(rsp.rspCode, 'D');
            assert.strictEqual(rsp.rspReason, 'ERR_ACCESS_SRV__PREP_SETUP_ACCESS_SRV__INV_PHONE');
            assert.strictEqual(rsp.rspMessage, 'The supplied phone ' + null + ' is invalid! It should be a none-empty string.');

            // 3.2. check that token was not generated

            let tokenFindResult = await tokenDao.findAll(trans, { user: "__super_user__" });

            assert.ok(tokenFindResult);
            assert.strictEqual(tokenFindResult.length, 0);

            // 3.3 check that notification was not created

            let notifFindResult = await notifDao.findAll(trans, { user: "__super_user__" });

            assert.ok(notifFindResult);
            assert.strictEqual(notifFindResult.length, 0);

            // 3.4 check that the request has been logged
            let logFindResult = await prepareSetupAccessLogDao.findByFilter(trans, { reqRef: req.reqRef }, { user: "__super_user__" });

            assert.ok(logFindResult);
            assert.strictEqual(logFindResult.length, 1);
            assert.strictEqual(logFindResult[0].rspCode, 'D');
            assert.strictEqual(logFindResult[0].rspReason, 'ERR_ACCESS_SRV__PREP_SETUP_ACCESS_SRV__INV_PHONE');
            assert.strictEqual(logFindResult[0].rspMessage, 'The supplied phone ' + null + ' is invalid! It should be a none-empty string.');
            assert.strictEqual(logFindResult[0].firstName, req.firstName);
            assert.strictEqual(logFindResult[0].reqRef, req.reqRef);

        });  

    it('\n' +
        '       | Conditions: \n' +
        '       | - email is string \n' +
        '       | - firstName is empty string \n' +
        '       | Expected :  \n' +
        '       | -> should return rspCode D(eclined).  \n' +
        '', async () => {

            // 1. prepare test-data 

            let req = {
                firstName: "",
                email: chance.string({ length: 10 }),
                reqRef: chance.string({ length: 10 })
            }

            let options = { user: "__super_user__" }

            // 2. execute test

            let rsp = await prepareSetupSomeAccessService.process(trans, req, options);

            // 3. compare outcome

            // 3.1. check the returned response        
            assert.ok(rsp);
            assert.strictEqual(rsp.rspCode, 'D');
            assert.strictEqual(rsp.rspReason, 'ERR_ACCESS_SRV__PREP_SETUP_ACCESS_SRV__INV_FIRST_NAME');
            assert.strictEqual(rsp.rspMessage, 'The supplied firstName ' + null + ' is invalid! It should be a none-empty string.');

            // 3.2. check that token was not generated

            let tokenFindResult = await tokenDao.findAll(trans, { user: "__super_user__" });

            assert.ok(tokenFindResult);
            assert.strictEqual(tokenFindResult.length, 0);

            // 3.3 check that notification was not created

            let notifFindResult = await notifDao.findAll(trans, { user: "__super_user__" });

            assert.ok(notifFindResult);
            assert.strictEqual(notifFindResult.length, 0);

            // 3.4 check that the request has been logged
            let logFindResult = await prepareSetupAccessLogDao.findByFilter(trans, { reqRef: req.reqRef }, { user: "__super_user__" });

            assert.ok(logFindResult);
            assert.strictEqual(logFindResult.length, 1);
            assert.strictEqual(logFindResult[0].rspCode, 'D');
            assert.strictEqual(logFindResult[0].rspReason, 'ERR_ACCESS_SRV__PREP_SETUP_ACCESS_SRV__INV_FIRST_NAME');
            assert.strictEqual(logFindResult[0].rspMessage, 'The supplied firstName ' + null + ' is invalid! It should be a none-empty string.');
            assert.strictEqual(logFindResult[0].firstName, req.firstName);
            assert.strictEqual(logFindResult[0].reqRef, req.reqRef);

        });  


    it('\n' +
        '       | Conditions: \n' +
        '       | - email is string \n' +
        '       | - lastName is empty string \n' +
        '       | Expected :  \n' +
        '       | -> should return rspCode D(eclined).  \n' +
        '', async () => {

            // 1. prepare test-data 

            let req = {
                lastName: "",
                email: chance.string({ length: 10 }),
                reqRef: chance.string({ length: 10 })
            }

            let options = { user: "__super_user__" }

            // 2. execute test

            let rsp = await prepareSetupSomeAccessService.process(trans, req, options);

            // 3. compare outcome

            // 3.1. check the returned response        
            assert.ok(rsp);
            assert.strictEqual(rsp.rspCode, 'D');
            assert.strictEqual(rsp.rspReason, 'ERR_ACCESS_SRV__PREP_SETUP_ACCESS_SRV__INV_LAST_NAME');
            assert.strictEqual(rsp.rspMessage, 'The supplied lastName ' + null + ' is invalid! It should be a none-empty string.');

            // 3.2. check that token was not generated

            let tokenFindResult = await tokenDao.findAll(trans, { user: "__super_user__" });

            assert.ok(tokenFindResult);
            assert.strictEqual(tokenFindResult.length, 0);

            // 3.3 check that notification was not created

            let notifFindResult = await notifDao.findAll(trans, { user: "__super_user__" });

            assert.ok(notifFindResult);
            assert.strictEqual(notifFindResult.length, 0);

            // 3.4 check that the request has been logged
            let logFindResult = await prepareSetupAccessLogDao.findByFilter(trans, { reqRef: req.reqRef }, { user: "__super_user__" });

            assert.ok(logFindResult);
            assert.strictEqual(logFindResult.length, 1);
            assert.strictEqual(logFindResult[0].rspCode, 'D');
            assert.strictEqual(logFindResult[0].rspReason, 'ERR_ACCESS_SRV__PREP_SETUP_ACCESS_SRV__INV_LAST_NAME');
            assert.strictEqual(logFindResult[0].rspMessage, 'The supplied lastName ' + null + ' is invalid! It should be a none-empty string.');
            assert.strictEqual(logFindResult[0].lastName, req.lastName);
            assert.strictEqual(logFindResult[0].reqRef, req.reqRef);

        });  


    it('\n' +
        '       | Conditions: \n' +
        '       | - email already in use \n' +
        '       | - firstName is valid \n' +
        '       | Expected :  \n' +
        '       | -> should return rspCode D(eclined).  \n' +
        '', async () => {

            // 1. prepare test-data 

            let req = {
                firstName: chance.string({length: 10}),
                email: chance.string({ length: 10 }),
                reqRef: chance.string({ length: 10 })
            }

            let req2 = {
                firstName: chance.string({ length: 10 }),
                email: req.email,
                reqRef: chance.string({ length: 10 })
            }

            let options = { user: "__super_user__" }

            // 2. execute test

            let rsp = await prepareSetupSomeAccessService.process(trans, req, options);

            assert.ok(rsp);
            assert.strictEqual(rsp.rspCode, 'A');

            let token = (await tokenDao.findByFilter(trans, {owner: req.email}, options))[0]

            let setupAccessReq = {
                password: chance.string({length: 60}),
                tokenCode: token.code
            }
            await setupAccessService.process(trans, setupAccessReq, options);

            let rsp2 = await prepareSetupSomeAccessService.process(trans, req2, options);

            // 3. compare outcome

            // 3.1. check the returned response        
            assert.ok(rsp2);
            assert.strictEqual(rsp2.rspCode, 'D');
            assert.strictEqual(rsp2.rspReason, 'ERR_ACCESS_SRV__PREP_SETUP_ACCESS_SRV__EMAIL_ALREADY_USED');
            assert.strictEqual(rsp2.rspMessage, 'The supplied email ' + req.email + ' is already used by another user! Please try another email.');

            // 3.2. check that token was not generated

            let tokenFindResult = await tokenDao.findAll(trans, { user: "__super_user__" });

            assert.ok(tokenFindResult);
            assert.strictEqual(tokenFindResult.length, 0);

            // 3.3 check that notification was created ( for first request only)

            let notifFindResult = await notifDao.findAll(trans, { user: "__super_user__" });

            assert.ok(notifFindResult);
            assert.strictEqual(notifFindResult.length, 1);

            // 3.4 check that the request has been logged
            let logFindResult = await prepareSetupAccessLogDao.findByFilter(trans, { reqRef: req2.reqRef }, { user: "__super_user__" });

            assert.ok(logFindResult);
            assert.strictEqual(logFindResult.length, 1);
            assert.strictEqual(logFindResult[0].rspCode, 'D');
            assert.strictEqual(logFindResult[0].rspReason, 'ERR_ACCESS_SRV__PREP_SETUP_ACCESS_SRV__EMAIL_ALREADY_USED');
            assert.strictEqual(logFindResult[0].rspMessage, 'The supplied email ' + req2.email + ' is already used by another user! Please try another email.');
            assert.strictEqual(logFindResult[0].firstName, req2.firstName);
            assert.strictEqual(logFindResult[0].reqRef, req2.reqRef);

        });  


    it('\n' +
        '       | Conditions: \n' +
        '       | - phone already in use \n' +
        '       | - firstName is valid \n' +
        '       | Expected :  \n' +
        '       | -> should return rspCode D(eclined).  \n' +
        '', async () => {

            // 1. prepare test-data 

            let req = {
                firstName: chance.string({ length: 10 }),
                phone: chance.string({ length: 10 }),
                reqRef: chance.string({ length: 10 })
            }

            let req2 = {
                firstName: chance.string({ length: 10 }),
                phone: req.phone,
                reqRef: chance.string({ length: 10 })
            }

            let options = { user: "__super_user__" }

            // 2. execute test

            let rsp = await prepareSetupSomeAccessService.process(trans, req, options);

            assert.ok(rsp);
            assert.strictEqual(rsp.rspCode, 'A');

            let token = (await tokenDao.findByFilter(trans, { owner: req.phone }, options))[0]

            let setupAccessReq = {
                password: chance.string({ length: 60 }),
                tokenCode: token.code
            }
            await setupAccessService.process(trans, setupAccessReq, options);

            let rsp2 = await prepareSetupSomeAccessService.process(trans, req2, options);

            // 3. compare outcome

            // 3.1. check the returned response        
            assert.ok(rsp2);
            assert.strictEqual(rsp2.rspCode, 'D');
            assert.strictEqual(rsp2.rspReason, 'ERR_ACCESS_SRV__PREP_SETUP_ACCESS_SRV__PHONE_ALREADY_USED');
            assert.strictEqual(rsp2.rspMessage, 'The supplied phone ' + req.phone + ' is already used by another user! Please try another phone.');

            // 3.2. check that token was not generated

            let tokenFindResult = await tokenDao.findAll(trans, { user: "__super_user__" });

            assert.ok(tokenFindResult);
            assert.strictEqual(tokenFindResult.length, 0);

            // 3.3 check that notification was created ( for first request only)

            let notifFindResult = await notifDao.findAll(trans, { user: "__super_user__" });

            assert.ok(notifFindResult);
            assert.strictEqual(notifFindResult.length, 1);

            // 3.4 check that the request has been logged
            let logFindResult = await prepareSetupAccessLogDao.findByFilter(trans, { reqRef: req2.reqRef }, { user: "__super_user__" });

            assert.ok(logFindResult);
            assert.strictEqual(logFindResult.length, 1);
            assert.strictEqual(logFindResult[0].rspCode, 'D');
            assert.strictEqual(logFindResult[0].rspReason, 'ERR_ACCESS_SRV__PREP_SETUP_ACCESS_SRV__PHONE_ALREADY_USED');
            assert.strictEqual(logFindResult[0].rspMessage, 'The supplied phone ' + req2.phone + ' is already used by another user! Please try another phone.');
            assert.strictEqual(logFindResult[0].firstName, req2.firstName);
            assert.strictEqual(logFindResult[0].reqRef, req2.reqRef);

        });  
});
