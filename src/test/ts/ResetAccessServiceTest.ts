// libs
import * as assert from "assert";
import { Chance } from "chance";
import Container from "@yinz/container/ts/Container";
import { TypeormDao, TypeormDataSource } from "@yinz/commons.data";
import { YinzConnection, YinzTransaction } from "@yinz/commons.data/ts/DataSource";
import User from "@yinz/access.data/models/User";
import { PasswordUtils } from "@yinz/commons";
import ArchToken from "@yinz/token.data/models/ArchToken";
import TokenManager from "@yinz/token.services/ts/TokenManager";
import ResetAccessService from "main/ts/ResetAccessService";
import ResetAccessLog from "@yinz/access.data/models/ResetAccessLog";


const container = new Container({
    paramsFile: 'params.yml',
    modules: [
        '@yinz/commons/ts',
        '@yinz/commons.data/ts',
        '@yinz/access.data/ts',
        '@yinz/commons.services/ts',
        '@yinz/token.data/ts',
        '@yinz/token.services/ts',
        process.cwd() + '/dist/main/ts'
    ],
});

const chance = new Chance();
// const Exception = container.getClazz('Exception');

const dataSource = container.getBean<TypeormDataSource>('typeormDataSource');


const userDao = container.getBean<TypeormDao<User>>('userDao');
const resetAccessLogDao = container.getBean<TypeormDao<ResetAccessLog>>('resetAccessLogDao');
const archTokenDao = container.getBean<TypeormDao<ArchToken>>('archTokenDao');

const tokenManager = container.getBean<TokenManager>("tokenManager");

const resetAccessService = container.getBean<ResetAccessService>("resetAccessService");


let conn: YinzConnection;
let trans: YinzTransaction;



describe('| access.services <ResetAccessService>', function () {

    before(() => {
        return new Promise(async (resolve) => {
            conn = await dataSource.getConn();
            resolve(conn);
        })
    });

    after(() => {
        return new Promise(async (resolve) => {
            await dataSource.relConn(conn)
            resolve(true);
        })
    });

    beforeEach(() => {
        return new Promise(async (resolve) => {
            trans = await dataSource.startTrans(conn);
            resolve(trans);
        })
    });

    afterEach(() => {
        return new Promise(async (resolve) => {
            await dataSource.rollbackTrans(trans)
            resolve(true);
        })
    });

    it('\n' +
        '       | Conditions: \n' +
        '       | - token is valid \n' +
        '       | - password is valid \n' +        
        '       | Expected :  \n' +
        '       | -> should return rspCode A(ccepted).  \n' +
        '       | -> should update a user  \n' +
        '       | -> should archive the token  \n' +
        '', async () => {

            // 1. prepare test data

            let userCode = chance.string({ length: 10 });
            let password = chance.string({ length: 10 });
            let tokenData = { }

            let options = { user: "__super_user__" };

            // 1.1 create the user 

            let user = new User();
            user.code = userCode;
            user.password = chance.string({length: 60});
            user.status = "I";
            user.wrongAttempts = chance.integer({min: 10, max: 100});            

            user = await userDao.create(trans, user, options);

            let token = await tokenManager.generateToken(trans, userCode, tokenData, options)
            

            let req = {
                reqUser: "__anonymous__",
                tokenCode: token.code,
                password: password,
                reqRef: chance.string({ length: 10 })
            }            

            // 2. execute test

            let rsp = await resetAccessService.process(trans, req, options);

            // 3. compare result

            // 3.1 check rsp
            assert.ok(rsp)
            assert.strictEqual(rsp.rspCode, "A");

            // 3.2 check that user was updated

            let userFindResult = await userDao.findByFilter(trans, { code: userCode }, options);

            assert.ok(userFindResult)
            assert.ok(userFindResult[0])
            assert.deepStrictEqual(userFindResult.length, 1)
            assert.deepStrictEqual(userFindResult[0].code, userCode)
            assert.deepStrictEqual(userFindResult[0].status, "A")
            assert.deepStrictEqual(userFindResult[0].wrongAttempts, 0)
            assert.deepStrictEqual(userFindResult[0].password, await PasswordUtils.hashPassword(password, userCode));

            // 3.3 check archived token

            let archTokenFindResult = await archTokenDao.findByFilter(trans, { code: token.code }, options);

            assert.ok(archTokenFindResult)
            assert.ok(archTokenFindResult[0])
            assert.ok(archTokenFindResult[0].archOn)
            assert.deepStrictEqual(archTokenFindResult.length, 1)
            assert.deepStrictEqual(archTokenFindResult[0].code, token.code)

            // 3.4 check that the request was logged              

            let resetAccessLogFindResult = await resetAccessLogDao.findByFilter(trans, { reqRef: req.reqRef }, options)

            assert.ok(resetAccessLogFindResult)
            assert.ok(resetAccessLogFindResult[0])
            assert.deepStrictEqual(resetAccessLogFindResult[0].reqUser, userCode)
            assert.deepStrictEqual(resetAccessLogFindResult[0].tokenCode, req.tokenCode)
            assert.deepStrictEqual(resetAccessLogFindResult[0].rspCode, "A")

        });



    it('\n' +
        '       | Conditions: \n' +
        '       | - token is valid \n' +
        '       | - passwordDigest is valid \n' +
        '       | Expected :  \n' +
        '       | -> should return rspCode A(ccepted).  \n' +
        '       | -> should update a user  \n' +
        '       | -> should archive the token  \n' +
        '', async () => {

            // 1. prepare test data

            let userCode = chance.string({ length: 10 });
            let password = chance.string({ length: 10 });
            let tokenData = { }

            let options = { user: "__super_user__" };

            // 1.1 create the user 

            let user = new User();
            user.code = userCode;
            user.password = chance.string({ length: 60 });
            user.status = "I";
            user.wrongAttempts = chance.integer({ min: 10, max: 100 });

            user = await userDao.create(trans, user, options);

            let token = await tokenManager.generateToken(trans, userCode, tokenData, options)


            let req = {
                reqUser: "__anonymous__",
                tokenCode: token.code,
                passwordDigest: await PasswordUtils.hashPassword(password, userCode),
                reqRef: chance.string({ length: 10 })
            }

            // 2. execute test

            let rsp = await resetAccessService.process(trans, req, options);

            // 3. compare result

            // 3.1 check rsp
            assert.ok(rsp)
            assert.strictEqual(rsp.rspCode, "A");

            // 3.2 check that user was updated

            let userFindResult = await userDao.findByFilter(trans, { code: userCode }, options);

            assert.ok(userFindResult)
            assert.ok(userFindResult[0])
            assert.deepStrictEqual(userFindResult.length, 1)
            assert.deepStrictEqual(userFindResult[0].code, userCode)
            assert.deepStrictEqual(userFindResult[0].status, "A")
            assert.deepStrictEqual(userFindResult[0].wrongAttempts, 0)
            assert.deepStrictEqual(userFindResult[0].password, await PasswordUtils.hashPassword(password, userCode));

            // 3.3 check archived token

            let archTokenFindResult = await archTokenDao.findByFilter(trans, { code: token.code }, options);

            assert.ok(archTokenFindResult)
            assert.ok(archTokenFindResult[0])
            assert.ok(archTokenFindResult[0].archOn)
            assert.deepStrictEqual(archTokenFindResult.length, 1)
            assert.deepStrictEqual(archTokenFindResult[0].code, token.code)

            // 3.4 check that the request was logged              

            let resetAccessLogFindResult = await resetAccessLogDao.findByFilter(trans, { reqRef: req.reqRef }, options)

            assert.ok(resetAccessLogFindResult)
            assert.ok(resetAccessLogFindResult[0])
            assert.deepStrictEqual(resetAccessLogFindResult[0].reqUser, userCode)
            assert.deepStrictEqual(resetAccessLogFindResult[0].tokenCode, req.tokenCode)
            assert.deepStrictEqual(resetAccessLogFindResult[0].rspCode, "A")

        });    


    it('\n' +
        '       | Conditions: \n' +
        '       | - token is not valid \n' +
        '       | - password is valid \n' +
        '       | Expected :  \n' +
        '       | -> should return rspCode D(eclined).  \n' +
        '       | -> should not update a user  \n' +        
        '       | -> rsp rsp.rspReason should be ERR_ACCESS_SRV__RESET_ACCESS_SRV__INV_TOKEN \n' +        
        '', async () => {

            // 1. prepare test data

            let userCode = chance.string({ length: 10 });
            let password = chance.string({ length: 10 });
            let tokenData = {}

            let options = { user: "__super_user__" };

            // 1.1 create the user 

            let user = new User();
            user.code = userCode;
            user.password = chance.string({ length: 60 });
            user.status = "I";
            user.wrongAttempts = chance.integer({ min: 10, max: 100 });

            user = await userDao.create(trans, user, options);

            let token = await tokenManager.generateToken(trans, userCode, tokenData, options)


            let req = {
                reqUser: "__anonymous__",
                tokenCode: chance.integer({min: 10, max: 100}) as any,
                password: password,
                reqRef: chance.string({ length: 10 })
            }

            // 2. execute test

            let rsp = await resetAccessService.process(trans, req, options);

            // 3. compare result

            // 3.1 check rsp
            assert.ok(rsp)
            assert.strictEqual(rsp.rspCode, "D");
            assert.strictEqual(rsp.rspReason, "ERR_ACCESS_SRV__RESET_ACCESS_SRV__INV_TOKEN");

            // 3.2 check that user was updated

            let userFindResult = await userDao.findByFilter(trans, { code: userCode }, options);

            assert.ok(userFindResult)
            assert.ok(userFindResult[0])
            assert.deepStrictEqual(userFindResult.length, 1)
            assert.deepStrictEqual(userFindResult[0].code, userCode)
            assert.deepStrictEqual(userFindResult[0].status, "I")
            assert.deepStrictEqual(userFindResult[0].wrongAttempts, user.wrongAttempts)
            assert.deepStrictEqual(userFindResult[0].password, user.password);

            // 3.3 check archived token

            let archTokenFindResult = await archTokenDao.findByFilter(trans, { code: token.code }, options);

            assert.ok(archTokenFindResult)            
            assert.deepStrictEqual(archTokenFindResult.length, 0)            
            
        });    


    it('\n' +
        '       | Conditions: \n' +
        '       | - token is valid \n' +
        '       | - password is invalid \n' +
        '       | Expected :  \n' +
        '       | -> should return rspCode D(eclined).  \n' +
        '       | -> should not update a user  \n' +
        '       | -> rsp rsp.rspReason should be ERR_ACCESS_SRV__RESET_ACCESS_SRV__INV_PWD \n' +
        '', async () => {

            // 1. prepare test data

            let userCode = chance.string({ length: 10 });
            // let password = chance.string({ length: 10 });
            let tokenData = {}

            let options = { user: "__super_user__" };

            // 1.1 create the user 

            let user = new User();
            user.code = userCode;
            user.password = chance.string({ length: 60 });
            user.status = "I";
            user.wrongAttempts = chance.integer({ min: 10, max: 100 });

            user = await userDao.create(trans, user, options);

            let token = await tokenManager.generateToken(trans, userCode, tokenData, options)


            let req = {
                reqUser: "__anonymous__",
                tokenCode: token.code,
                password: chance.integer({ min: 10, max: 100 }) as any,
                reqRef: chance.string({ length: 10 })
            }

            // 2. execute test

            let rsp = await resetAccessService.process(trans, req, options);

            // 3. compare result

            // 3.1 check rsp
            assert.ok(rsp)
            assert.strictEqual(rsp.rspCode, "D");
            assert.strictEqual(rsp.rspReason, "ERR_ACCESS_SRV__RESET_ACCESS_SRV__INV_PWD");

            // 3.2 check that user was updated

            let userFindResult = await userDao.findByFilter(trans, { code: userCode }, options);

            assert.ok(userFindResult)
            assert.ok(userFindResult[0])
            assert.deepStrictEqual(userFindResult.length, 1)
            assert.deepStrictEqual(userFindResult[0].code, userCode)
            assert.deepStrictEqual(userFindResult[0].status, "I")
            assert.deepStrictEqual(userFindResult[0].wrongAttempts, user.wrongAttempts)
            assert.deepStrictEqual(userFindResult[0].password, user.password);

            // 3.3 check archived token

            let archTokenFindResult = await archTokenDao.findByFilter(trans, { code: token.code }, options);

            assert.ok(archTokenFindResult)
            assert.deepStrictEqual(archTokenFindResult.length, 0)

        });   
});
