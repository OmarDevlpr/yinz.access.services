// libs
import * as assert from "assert";
import { Chance } from "chance";
import Container from "@yinz/container/ts/Container";
import { TypeormDao, TypeormDataSource } from "@yinz/commons.data";
import { YinzConnection, YinzTransaction } from "@yinz/commons.data/ts/DataSource";
import Authz from "@yinz/access.data/models/Authz";
import Profile from "@yinz/access.data/models/Profile";
import User from "@yinz/access.data/models/User";
import PrepareResetAccessLog from "@yinz/access.data/models/PrepareResetAccessLog";
import Token from "@yinz/token.data/models/Token";
import Notif from "@yinz/notif.data/models/Notif";
import PrepareResetAccessService from "../../main/ts/PrepareResetAccessService";
import TokenManager from "@yinz/token.services/ts/TokenManager";
import SetupAccessService from "../../main/ts/SetupAccessService";
import ProfileAuthz from "@yinz/access.data/models/ProfileAuthz";


const container = new Container({
    paramsFile: 'params.yml',
    modules: [
        '@yinz/commons/ts',
        '@yinz/commons.data/ts',
        '@yinz/access.data/ts',
        '@yinz/commons.services/ts',
        '@yinz/token.data/ts',
        '@yinz/token.services/ts',
        '@yinz/notif.data/ts',
        '@yinz/notif.services/ts',
        process.cwd() + '/dist/main/ts'
    ],
});

const chance = new Chance();
// const Exception = container.getClazz('Exception');

const dataSource = container.getBean<TypeormDataSource>('typeormDataSource');



const authzDao = container.getBean<TypeormDao<Authz>>('authzDao');
const profileAuthzDao = container.getBean<TypeormDao<ProfileAuthz>>('profileAuthzDao');
const userDao = container.getBean<TypeormDao<User>>('userDao');
const profileDao = container.getBean<TypeormDao<Profile>>('profileDao');

const notifDao = container.getBean<TypeormDao<Notif>>('notifDao');
const tokenDao = container.getBean<TypeormDao<Token>>('tokenDao');
const prepareResetAccessLogDao = container.getBean<TypeormDao<PrepareResetAccessLog>>('prepareResetAccessLogDao');
const setupAccessService = container.getBean<SetupAccessService>("setupAccessService");
const tokenManager = container.getBean<TokenManager>("tokenManager");

const prepareResetAccessService = container.getBean<PrepareResetAccessService>("prepareResetAccessService");



let conn: YinzConnection;
let trans: YinzTransaction;



describe('| access.services <PrepareResetAccessService>', function () {

    let profiles = [
        { code: chance.string({ length: 10 }) },
        { code: chance.string({ length: 10 }) },
        { code: chance.string({ length: 10 }) },
    ] as Profile[];
    let authzes = [
        { code: 'PrepareResetAccessService' },
    ] as Authz[];

    before(() => {
        return new Promise(async (resolve) => {
            conn = await dataSource.getConn();
            resolve(conn);
        })
    });

    after(() => {
        return new Promise(async (resolve) => {
            await dataSource.relConn(conn)
            resolve(true);
        })
    });

    beforeEach(() => {
        return new Promise(async (resolve) => {
            trans = await dataSource.startTrans(conn);

            // 1. create authz 
            authzes = await authzDao.createAll(trans, authzes, { user: "__super_user__" })

            // 2. create profile 
            profiles = await profileDao.createAll(trans, profiles, { user: "__super_user__", include: [] })

            for (let profile of profiles) {
                for (let authz of authzes) {
                    
                    // 3. link between them
                    await profileAuthzDao.create(trans, { authz, allowAll: true, profile }, { user: "__super_user__", include: ['authz', 'profile'] })
                }
            }
            resolve(trans);
        })
    });

    afterEach(() => {
        return new Promise(async (resolve) => {
            await dataSource.rollbackTrans(trans)
            resolve(true);
        })
    });


    it('\n' +
        '       | Conditions: \n' +
        '       | - userCode exists \n' +
        '       | - user exists \n' +
        '       | Expected :  \n' +
        '       | -> should return rspCode A(ccepted).  \n' +
        '', async () => {

            // 1. prepare test-data 
            let reqRef = chance.string({ length: 10 });
            let reqUser = chance.email();
            let data = { setupMode: 'Automatic' };
            let password = chance.string({ length: 6, pool: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' });            

            // 1.1 generate user token
            let token = await tokenManager.generateToken(trans, reqUser, data, { user: "__super_user__" });

            // 1.2 setup user access
            await setupAccessService.process(trans, { tokenCode: token.code, password: password }, { user: "__super_user__" });            

            // 1.3 update the user profiles                                    
            await userDao.updateByFilter(trans, { profiles }, { code: reqUser }, { include: ['profiles'], user: "__super_user__" });

            // 1.4 PrepareResetAccessService req

            let req = {
                userCode: reqUser,
                reqLoc: '1.1.1.1',
                reqRef
            }

            let options = {
                user: "__super_user__",
                model: {
                    reqLoc: req.reqLoc
                }
            }

            // 2. execute test

            let rsp = await prepareResetAccessService.process(trans, req, options);

            // 3. compare outcome

            // 3.1. check the returned response        
            assert.ok(rsp);
            assert.strictEqual(rsp.rspCode, 'A');

            // 3.2. check that token was generated

            let tokenFindResult = await tokenDao.findAll(trans, { user: "__super_user__" });

            assert.ok(tokenFindResult);
            assert.strictEqual(tokenFindResult.length, 1);
            assert.strictEqual(tokenFindResult[0].owner, req.userCode);
            assert.deepStrictEqual(tokenFindResult[0].data, null);
            assert.strictEqual(tokenFindResult[0].maxNumberOfUse, 1);
            assert.strictEqual(tokenFindResult[0].numberOfUse, 0);

            // 3.3 check that notification was created

            let notifFindResult = await notifDao.findAll(trans, { user: "__super_user__" });

            assert.ok(notifFindResult);
            assert.strictEqual(notifFindResult.length, 1);
            assert.strictEqual(notifFindResult[0].dest, req.userCode);
            assert.strictEqual(notifFindResult[0].destType, 'E');
            assert.strictEqual(notifFindResult[0].reason, 'RESET_PASSWORD');
            assert.strictEqual(notifFindResult[0].subject, 'resetAccessInstructionsMessageSubject');
            assert.strictEqual(notifFindResult[0].message, 'resetAccessInstructionsMessageBody');

            // 3.4 check that the request has been logged
            let logFindResult = await prepareResetAccessLogDao.findByFilter(trans, { reqRef: req.reqRef }, { user: "__super_user__" });

            assert.ok(logFindResult);
            assert.strictEqual(logFindResult.length, 1);
            assert.strictEqual(logFindResult[0].rspCode, 'A');

            assert.strictEqual(logFindResult[0].userCode, req.userCode);            
            assert.strictEqual(logFindResult[0].reqRef, req.reqRef);
            assert.strictEqual(logFindResult[0].reqLoc, req.reqLoc);

        });

    it('\n' +
        '       | Conditions: \n' +
        '       | - userCode exists and is a phone \n' +
        '       | - user exists \n' +
        '       | Expected :  \n' +
        '       | -> should return rspCode A(ccepted).  \n' +
        '', async () => {

            // 1. prepare test-data 
            let reqRef = chance.string({ length: 10 });
            let reqUser = "0" + chance.integer({min: 10000000, max: 99999999});
            let data = { setupMode: 'Automatic' };
            let password = chance.string({ length: 6, pool: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' });

            // 1.1 generate user token
            let token = await tokenManager.generateToken(trans, reqUser, data, { user: "__super_user__" });

            // 1.2 setup user access
            await setupAccessService.process(trans, { tokenCode: token.code, password: password }, { user: "__super_user__" });

            // 1.3 update the user profiles                                    
            await userDao.updateByFilter(trans, { profiles }, { code: reqUser }, { include: ['profiles'], user: "__super_user__" });

            // 1.4 PrepareResetAccessService req

            let req = {
                userCode: reqUser,
                reqLoc: '1.1.1.1',
                reqRef
            }

            let options = {
                user: "__super_user__"
            }

            // 2. execute test

            let rsp = await prepareResetAccessService.process(trans, req, options);

            // 3. compare outcome

            // 3.1. check the returned response        
            assert.ok(rsp);
            assert.strictEqual(rsp.rspCode, 'A');

            // 3.2. check that token was generated

            let tokenFindResult = await tokenDao.findAll(trans, { user: "__super_user__" });

            assert.ok(tokenFindResult);
            assert.strictEqual(tokenFindResult.length, 1);
            assert.strictEqual(tokenFindResult[0].owner, req.userCode);
            assert.deepStrictEqual(tokenFindResult[0].data, null);
            assert.strictEqual(tokenFindResult[0].maxNumberOfUse, 1);
            assert.strictEqual(tokenFindResult[0].numberOfUse, 0);

            // 3.3 check that notification was created

            let notifFindResult = await notifDao.findAll(trans, { user: "__super_user__" });

            assert.ok(notifFindResult);
            assert.strictEqual(notifFindResult.length, 1);
            assert.strictEqual(notifFindResult[0].dest, req.userCode);
            assert.strictEqual(notifFindResult[0].destType, 'S');
            assert.strictEqual(notifFindResult[0].reason, 'RESET_PASSWORD');
            assert.strictEqual(notifFindResult[0].subject, 'resetAccessInstructionsMessageSubject');
            assert.strictEqual(notifFindResult[0].message, 'resetAccessInstructionsMessageBody');

            // 3.4 check that the request has been logged
            let logFindResult = await prepareResetAccessLogDao.findByFilter(trans, { reqRef: req.reqRef }, { user: "__super_user__" });

            assert.ok(logFindResult);
            assert.strictEqual(logFindResult.length, 1);
            assert.strictEqual(logFindResult[0].rspCode, 'A');

            assert.strictEqual(logFindResult[0].userCode, req.userCode);
            assert.strictEqual(logFindResult[0].reqRef, req.reqRef);
            assert.strictEqual(logFindResult[0].reqLoc, req.reqLoc);

        });

    it('\n' +
        '       | Conditions: \n' +
        '       | - userCode is empty string \n' +
        '       | - user exists \n' +
        '       | Expected :  \n' +
        '       | -> should return rspCode D(eclined).  \n' +
        '', async () => {

            // 1. prepare test-data 
            let reqRef = chance.string({ length: 10 });
            let reqUser = chance.email();
            let data = { setupMode: 'Automatic' };
            let password = chance.string({ length: 6, pool: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' });

            // 1.1 generate user token
            let token = await tokenManager.generateToken(trans, reqUser, data, { user: "__super_user__" });

            // 1.2 setup user access
            await setupAccessService.process(trans, { tokenCode: token.code, password: password }, { user: "__super_user__" });

            // 1.3 update the user profiles                                    
            await userDao.updateByFilter(trans, { profiles }, { code: reqUser }, { include: ['profiles'], user: "__super_user__" });

            // 1.4 PrepareResetAccessService req

            let req = {
                userCode: "",
                reqLoc: '1.1.1.1',
                reqRef
            }

            let options = {
                user: "__super_user__"
            }

            // 2. execute test

            let rsp = await prepareResetAccessService.process(trans, req, options);

            // 3. compare outcome

            // 3.1. check the returned response        
            assert.ok(rsp);
            assert.strictEqual(rsp.rspCode, 'D');
            assert.strictEqual(rsp.rspReason, 'ERR_ACCESS_SRV__PRP_RESET_ACCESS_SRV__INV_USER_CODE');

            // 3.2. check that token was generated

            let tokenFindResult = await tokenDao.findAll(trans, { user: "__super_user__" });

            assert.ok(tokenFindResult);
            assert.strictEqual(tokenFindResult.length, 0);            

            // 3.3 check that notification was created

            let notifFindResult = await notifDao.findAll(trans, { user: "__super_user__" });

            assert.ok(notifFindResult);
            assert.strictEqual(notifFindResult.length, 0);            

            // 3.4 check that the request has been logged
            let logFindResult = await prepareResetAccessLogDao.findByFilter(trans, { reqRef: req.reqRef }, { user: "__super_user__" });

            assert.ok(logFindResult);
            assert.strictEqual(logFindResult.length, 1);
            assert.strictEqual(logFindResult[0].rspCode, 'D');
            assert.strictEqual(logFindResult[0].rspReason, 'ERR_ACCESS_SRV__PRP_RESET_ACCESS_SRV__INV_USER_CODE');            

            assert.strictEqual(logFindResult[0].userCode, req.userCode);
            assert.strictEqual(logFindResult[0].reqRef, req.reqRef);
            assert.strictEqual(logFindResult[0].reqLoc, req.reqLoc);

        });

    it('\n' +
        '       | Conditions: \n' +
        '       | - userCode is nor an email or an sms \n' +
        '       | - user exists \n' +
        '       | Expected :  \n' +
        '       | -> should return rspCode D(eclined).  \n' +
        '', async () => {

            // 1. prepare test-data 
            let reqRef = chance.string({ length: 10 });
            let reqUser = chance.string({ length: 10, pool: "ABCDEFGHIJKLMNOPQRSTUVWXYZ"});
            let data = { setupMode: 'Automatic' };
            let password = chance.string({ length: 6, pool: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' });

            // 1.1 generate user token
            let token = await tokenManager.generateToken(trans, reqUser, data, { user: "__super_user__" });

            // 1.2 setup user access
            await setupAccessService.process(trans, { tokenCode: token.code, password: password }, { user: "__super_user__" });

            // 1.3 update the user profiles                                    
            await userDao.updateByFilter(trans, { profiles }, { code: reqUser }, { include: ['profiles'], user: "__super_user__" });

            // 1.4 PrepareResetAccessService req

            let req = {
                userCode: reqUser,
                reqLoc: '1.1.1.1',
                reqRef
            }

            let options = {
                user: "__super_user__"               
            }

            // 2. execute test

            let rsp = await prepareResetAccessService.process(trans, req, options);

            // 3. compare outcome

            // 3.1. check the returned response        
            assert.ok(rsp);
            assert.strictEqual(rsp.rspCode, 'D');
            assert.strictEqual(rsp.rspReason, 'ERR_ACCESSS_SRV__SND_RST_ACCSS_INSTRCT__INV_DST_TYPE');

            // 3.2. check that token was generated

            let tokenFindResult = await tokenDao.findAll(trans, { user: "__super_user__" });

            assert.ok(tokenFindResult);
            assert.strictEqual(tokenFindResult.length, 1);

            // 3.3 check that notification was created

            let notifFindResult = await notifDao.findAll(trans, { user: "__super_user__" });

            assert.ok(notifFindResult);
            assert.strictEqual(notifFindResult.length, 0);

            // 3.4 check that the request has been logged
            let logFindResult = await prepareResetAccessLogDao.findByFilter(trans, { reqRef: req.reqRef }, { user: "__super_user__" });

            assert.ok(logFindResult);
            assert.strictEqual(logFindResult.length, 1);
            assert.strictEqual(logFindResult[0].rspCode, 'D');
            assert.strictEqual(logFindResult[0].rspReason, 'ERR_ACCESSS_SRV__SND_RST_ACCSS_INSTRCT__INV_DST_TYPE');

            assert.strictEqual(logFindResult[0].userCode, req.userCode);
            assert.strictEqual(logFindResult[0].reqRef, req.reqRef);
            assert.strictEqual(logFindResult[0].reqLoc, req.reqLoc);

        });



    it('\n' +
        '       | Conditions: \n' +
        '       | - userCode is valid \n' +
        `       | - user doesn't exist \n` +
        '       | Expected :  \n' +
        '       | -> should return rspCode D(eclined).  \n' +
        '', async () => {

            // 1. prepare test-data 
            let reqRef = chance.string({ length: 10 });
            
            // 1.4 PrepareResetAccessService req

            let req = {
                userCode: chance.email(),
                reqLoc: '1.1.1.1',
                reqRef
            }

            let options = {
                user: "__super_user__"
            }

            // 2. execute test

            let rsp = await prepareResetAccessService.process(trans, req, options);

            // 3. compare outcome

            // 3.1. check the returned response        
            assert.ok(rsp);
            assert.strictEqual(rsp.rspCode, 'D');
            assert.strictEqual(rsp.rspReason, 'ERR_ACCESSS_SRV__PRP_RST_ACCSS__USER_NOT_FOUND');

            // 3.2. check that token was generated

            let tokenFindResult = await tokenDao.findAll(trans, { user: "__super_user__" });

            assert.ok(tokenFindResult);
            assert.strictEqual(tokenFindResult.length, 0);

            // 3.3 check that notification was created

            let notifFindResult = await notifDao.findAll(trans, { user: "__super_user__" });

            assert.ok(notifFindResult);
            assert.strictEqual(notifFindResult.length, 0);

            // 3.4 check that the request has been logged
            let logFindResult = await prepareResetAccessLogDao.findByFilter(trans, { reqRef: req.reqRef }, { user: "__super_user__" });

            assert.ok(logFindResult);
            assert.strictEqual(logFindResult.length, 1);
            assert.strictEqual(logFindResult[0].rspCode, 'D');
            assert.strictEqual(logFindResult[0].rspReason, 'ERR_ACCESSS_SRV__PRP_RST_ACCSS__USER_NOT_FOUND');

            assert.strictEqual(logFindResult[0].userCode, req.userCode);
            assert.strictEqual(logFindResult[0].reqRef, req.reqRef);
            assert.strictEqual(logFindResult[0].reqLoc, req.reqLoc);

        });
});
