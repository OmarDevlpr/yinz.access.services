// libs
import * as assert from "assert";
import { Chance } from "chance";
import Container from "@yinz/container/ts/Container";
import { TypeormDao, TypeormDataSource } from "@yinz/commons.data";
import { YinzConnection, YinzTransaction } from "@yinz/commons.data/ts/DataSource";
import Authz from "@yinz/access.data/models/Authz";
import Profile from "@yinz/access.data/models/Profile";
import User from "@yinz/access.data/models/User";
// import LoginService from "../../main/ts/LoginService";
// import { PasswordUtils } from "@yinz/commons";
import ChangePasswordLog from "@yinz/access.data/models/ChangePasswordLog";
import TokenManager from "@yinz/token.services/ts/TokenManager";
import SetupAccessService from "../../main/ts/SetupAccessService";
import ChangePasswordService from "../../main/ts/ChangePasswordService";
import SetupAccess from "@yinz/access.data/models/SetupAccess";
import ProfileAuthz from "@yinz/access.data/models/ProfileAuthz";


const container = new Container({
    paramsFile: 'params.yml',
    modules: [
        '@yinz/commons/ts',
        '@yinz/commons.data/ts',
        '@yinz/access.data/ts',
        '@yinz/token.data/ts',
        '@yinz/token.services/ts',
        '@yinz/commons.services/ts',
        process.cwd() + '/dist/main/ts'
    ],
});

const chance = new Chance();
// const Exception = container.getClazz('Exception');

const dataSource = container.getBean<TypeormDataSource>('typeormDataSource');


const authzDao = container.getBean<TypeormDao<Authz>>('authzDao');
const profileAuthzDao = container.getBean<TypeormDao<ProfileAuthz>>('profileAuthzDao');
const userDao = container.getBean<TypeormDao<User>>('userDao');
const profileDao = container.getBean<TypeormDao<Profile>>('profileDao');
const setupAccessDao = container.getBean<TypeormDao<SetupAccess>>('setupAccessDao');
const changePasswordLogDao = container.getBean<TypeormDao<ChangePasswordLog>>('changePasswordLogDao');

const setupAccessService = container.getBean<SetupAccessService>("setupAccessService");
const tokenManager = container.getBean<TokenManager>("tokenManager");
const changePasswordService = container.getBean<ChangePasswordService>("changePasswordService");



let conn: YinzConnection;
let trans: YinzTransaction;

describe('| access.services <ChangePasswordService>', function () {

    let profiles = [
        { code: chance.string({ length: 10 }) },
        { code: chance.string({ length: 10 }) },
        { code: chance.string({ length: 10 }) },
    ] as Profile[];
    let authzes = [
        { code: 'ChangePasswordService' },
    ] as Authz[];

    before(() => {
        return new Promise(async (resolve) => {
            conn = await dataSource.getConn();
            resolve(conn);
        })
    });

    after(() => {
        return new Promise(async (resolve) => {
            await dataSource.relConn(conn)
            resolve(true);
        })
    });

    beforeEach(() => {
        return new Promise(async (resolve) => {
            trans = await dataSource.startTrans(conn);
            
            // 1. create authz 
            authzes = await authzDao.createAll(trans, authzes, {user: "__super_user__"})

            // 2. create profile 
            profiles = await profileDao.createAll(trans, profiles, { user: "__super_user__", include: [] })
            
            for ( let profile of profiles) {                
                for ( let authz of authzes ) {                     

                    // 3. link between them
                    await profileAuthzDao.create(trans, { authz, allowAll: true, profile }, {user: "__super_user__", include: ['authz', 'profile']})                    
                }
            }                        
            resolve(trans);
        })
    });

    afterEach(() => {
        return new Promise(async (resolve) => {
            await dataSource.rollbackTrans(trans)
            resolve(true);
        })
    });



    it('\n' +
        '       | Conditions: \n' +
        '       | - userCode is valid \n' +
        '       | - oldPassword is valid \n' +
        '       | - newPassword is valid \n' +
        '       | - user.wrongAttempts is 0  \n' +
        '       | - user.status is A(ctive)  \n' +
        '       | - maxWrongAttempts is 3  \n' +
        '       | Expected :  \n' +
        '       | -> should return rspCode A(ccepted).  \n' +
        '', async () => {

            // 1. prepare test-data 
            let reqRef        = chance.string({length: 10});
            let reqUser      = chance.string({length: 10});
            let data          = {setupMode: 'Automatic'};
            let oldPassword   = chance.string({length: 6, pool: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'});
            let newPassword   = chance.string({length: 6, pool: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'});

            // 1.1 generate user token
            let token = await tokenManager.generateToken(trans, reqUser, data, {user: "__super_user__"});

            // 1.2 setup user access
            await setupAccessService.process(trans, {tokenCode: token.code, password: oldPassword}, {user: "__super_user__"});            

            // 1.3 update the user profiles                                    
            await userDao.updateByFilter(trans, { profiles }, { code: reqUser}, {include: ['profiles'], user: "__super_user__"});

            // 1.4 changePAssword service request
            let req = { reqRef, reqUser, oldPassword, newPassword};

            // 2. execute test
            let changePwdSrvRsp = await changePasswordService.process(trans, req, {user: "__super_user__"})

            // 3. check result 
            // 3.1 check return response

            assert.ok(changePwdSrvRsp);
            assert.strictEqual(changePwdSrvRsp.rspCode, 'A');
             

            // 3.2 check user WrongAttempt and status

            let userResult = await userDao.findByFilter(trans, {code: reqUser}, {user: "__super_user__"});
            assert.strictEqual(userResult.length, 1)
            assert.strictEqual(userResult[0].status, 'A')
            assert.strictEqual(userResult[0].wrongAttempts, 0)
            
            // 3.3 setupAccess should not be created
            let setupAccessResult = await setupAccessDao.findAll(trans, { user: "__super_user__" });
            assert.strictEqual(setupAccessResult.length, 0)
            
            // 3.4 check that request has been logged
            let logResult = await changePasswordLogDao.findAll(trans, {user: "__super_user__"});

            assert.ok(logResult);
            assert.strictEqual(logResult.length, 1);
            assert.strictEqual(logResult[0].rspCode, 'A');
            assert.ok(logResult[0].oldPassword !== oldPassword);
            assert.strictEqual(logResult[0].oldPassword.length, 60);
            assert.ok(logResult[0].newPassword !== newPassword);
            assert.strictEqual(logResult[0].newPassword.length, 60);




        });



    it('\n' +
        '       | Conditions: \n' +
        '       | - userCode is valid \n' +
        '       | - oldPasswordDigest is valid \n' +
        '       | - newPasswordDigest is valid \n' +
        '       | - user.wrongAttempts is 0  \n' +
        '       | - user.status is A(ctive)  \n' +
        '       | - maxWrongAttempts is 3  \n' +
        '       | Expected :  \n' +
        '       | -> should return rspCode A(ccepted).  \n' +
        '', async () => {

            // 1. prepare test-data 
            let reqRef = chance.string({ length: 10 });
            let reqUser = chance.string({ length: 10 });
            let data = { setupMode: 'Automatic' };
            let oldPasswordDigest = chance.string({ length: 6, pool: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' });
            let newPasswordDigest = chance.string({ length: 6, pool: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' });

            // 1.1 generate user token
            let token = await tokenManager.generateToken(trans, reqUser, data, { user: "__super_user__" });

            // 1.2 setup user access
            await setupAccessService.process(trans, { tokenCode: token.code, passwordDigest: oldPasswordDigest }, { user: "__super_user__" });

            // 1.3 update the user profiles
            await userDao.updateByFilter(trans, { profiles }, { code: reqUser }, { include: ['profiles'], user: "__super_user__" });

            // 1.4 changePAssword service request
            let req = { reqRef, reqUser, oldPasswordDigest: oldPasswordDigest, newPasswordDigest: newPasswordDigest };

            // 2. execute test
            let changePwdSrvRsp = await changePasswordService.process(trans, req, { user: "__super_user__" })

            // 3. check result 
            // 3.1 check return response

            assert.ok(changePwdSrvRsp);
            assert.strictEqual(changePwdSrvRsp.rspCode, 'A');


            // 3.2 check user WrongAttempt and status

            let userResult = await userDao.findByFilter(trans, { code: reqUser }, { user: "__super_user__" });
            assert.strictEqual(userResult.length, 1)
            assert.strictEqual(userResult[0].status, 'A')
            assert.strictEqual(userResult[0].wrongAttempts, 0)

            // 3.3 setupAccess should not be created
            let setupAccessResult = await setupAccessDao.findAll(trans, { user: "__super_user__" });
            assert.strictEqual(setupAccessResult.length, 0)

            // 3.4 check that request has been logged
            let logResult = await changePasswordLogDao.findAll(trans, { user: "__super_user__" });

            assert.ok(logResult);
            assert.strictEqual(logResult.length, 1);
            assert.strictEqual(logResult[0].rspCode, 'A');
            assert.ok(logResult[0].oldPassword === oldPasswordDigest);
            assert.strictEqual(logResult[0].oldPassword.length, oldPasswordDigest.length);
            assert.ok(logResult[0].newPassword === newPasswordDigest);
            assert.strictEqual(logResult[0].newPassword.length, oldPasswordDigest.length);

        });
    
    it('\n' +
        '       | Conditions: \n' +
        '       | - userCode is valid \n' +
        '       | - oldPassword is valid \n' +
        '       | - newPassword is valid \n' +
        '       | - user.wrongAttempts is 1  \n' +
        '       | - user.status is A(ctive)  \n' +
        '       | - maxWrongAttempts is 3  \n' +
        '       | Expected :  \n' +
        '       | -> should return rspCode A(ccepted).  \n' +
        '       | -> should reset wrong attempts to 0.  \n' +
        '', async () => {

            // 1. prepare test-data 
            let reqRef = chance.string({ length: 10 });
            let reqUser = chance.string({ length: 10 });
            let data = { setupMode: 'Automatic' };
            let oldPassword = chance.string({ length: 6, pool: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' });
            let newPassword = chance.string({ length: 6, pool: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' });

            // 1.1 generate user token
            let token = await tokenManager.generateToken(trans, reqUser, data, { user: "__super_user__" });

            // 1.2 setup user access
            await setupAccessService.process(trans, { tokenCode: token.code, password: oldPassword }, { user: "__super_user__" });

            // 1.3 update the user profiles
            await userDao.updateByFilter(trans, { profiles, wrongAttempts: 1  }, { code: reqUser}, { include: ['profiles'], user: "__super_user__" });

            // 1.4 changePAssword service request
            let req = { reqRef, reqUser, oldPassword, newPassword };

            // 2. execute test
            let changePwdSrvRsp = await changePasswordService.process(trans, req, { user: "__super_user__" })

            // 3. check result 
            // 3.1 check return response

            assert.ok(changePwdSrvRsp);
            assert.strictEqual(changePwdSrvRsp.rspCode, 'A');


            // 3.2 check user WrongAttempt and status

            let userResult = await userDao.findByFilter(trans, { code: reqUser }, { user: "__super_user__" });
            assert.strictEqual(userResult.length, 1)
            assert.strictEqual(userResult[0].status, 'A')
            assert.strictEqual(userResult[0].wrongAttempts, 0)

            // 3.3 setupAccess should not be created
            let setupAccessResult = await setupAccessDao.findAll(trans, { user: "__super_user__" });
            assert.strictEqual(setupAccessResult.length, 0)

            // 3.4 check that request has been logged
            let logResult = await changePasswordLogDao.findAll(trans, { user: "__super_user__" });

            assert.ok(logResult);
            assert.strictEqual(logResult.length, 1);
            assert.strictEqual(logResult[0].rspCode, 'A');
            assert.ok(logResult[0].oldPassword !== oldPassword);
            assert.strictEqual(logResult[0].oldPassword.length, 60);
            assert.ok(logResult[0].newPassword !== newPassword);
            assert.strictEqual(logResult[0].newPassword.length, 60);

        });


    it('\n' +
        '       | Conditions: \n' +
        '       | - userCode is invalid \n' +
        '       | - oldPassword is valid \n' +
        '       | - newPassword is valid \n' +
        '       | - user.wrongAttempts is 0  \n' +
        '       | - user.status is A(ctive)  \n' +
        '       | - maxWrongAttempts is 3  \n' +
        '       | Expected :  \n' +
        '       | -> should return rspCode D(eclined).  \n' +        
        '', async () => {

            // 1. prepare test-data 
            let reqRef = chance.string({ length: 10 });
            let reqUser = chance.string({ length: 10 });
            let data = { setupMode: 'Automatic' };
            let oldPassword = chance.string({ length: 6, pool: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' });
            let newPassword = chance.string({ length: 6, pool: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' });

            // 1.1 generate user token
            let token = await tokenManager.generateToken(trans, reqUser, data, { user: "__super_user__" });

            // 1.2 setup user access
            await setupAccessService.process(trans, { tokenCode: token.code, password: oldPassword }, { user: "__super_user__" });

            // 1.3 update the user profiles
            await userDao.updateByFilter(trans, { profiles }, { code: reqUser, wrongAttempts: 1 }, { include: ['profiles'], user: "__super_user__" });

            // 1.4 changePAssword service request
            let req = { reqRef, reqUser: "invalid", oldPassword, newPassword };

            // 2. execute test
            let changePwdSrvRsp = await changePasswordService.process(trans, req, { user: "__super_user__" })

            // 3. check result 
            // 3.1 check return response

            assert.ok(changePwdSrvRsp);
            assert.strictEqual(changePwdSrvRsp.rspCode, 'D');
            assert.strictEqual(changePwdSrvRsp.rspReason, 'ERR_ACCESS_SRV__CHNG_PWD_SRV__ACCESS');


            // 3.2 check user WrongAttempt and status

            let userResult = await userDao.findByFilter(trans, { code: reqUser }, { user: "__super_user__" });
            assert.strictEqual(userResult.length, 1)
            assert.strictEqual(userResult[0].status, 'A')
            assert.strictEqual(userResult[0].wrongAttempts, 0)

            // 3.3 setupAccess should not be created
            let setupAccessResult = await setupAccessDao.findAll(trans, { user: "__super_user__" });
            assert.strictEqual(setupAccessResult.length, 0)

            // 3.4 check that request has been logged
            let logResult = await changePasswordLogDao.findAll(trans, { user: "__super_user__" });

            assert.ok(logResult);
            assert.strictEqual(logResult.length, 1);
            assert.strictEqual(logResult[0].rspCode, 'D');
            assert.strictEqual(logResult[0].rspReason, 'ERR_ACCESS_SRV__CHNG_PWD_SRV__ACCESS');
            assert.ok(logResult[0].oldPassword !== oldPassword);
            assert.strictEqual(logResult[0].oldPassword.length, 60);
            assert.ok(logResult[0].newPassword !== newPassword);
            assert.strictEqual(logResult[0].newPassword.length, 60);

        });

    it('\n' +
        '       | Conditions: \n' +
        '       | - userCode is valid \n' +
        '       | - oldPassword is empty string \n' +
        '       | - newPassword is valid \n' +
        '       | - user.wrongAttempts is 0  \n' +
        '       | - user.status is A(ctive)  \n' +
        '       | - maxWrongAttempts is 3  \n' +
        '       | Expected :  \n' +
        '       | -> should return rspCode D(eclined).  \n' +        
        '', async () => {

            // 1. prepare test-data 
            let reqRef = chance.string({ length: 10 });
            let reqUser = chance.string({ length: 10 });
            let data = { setupMode: 'Automatic' };
            let oldPassword = chance.string({ length: 6, pool: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' });
            let newPassword = chance.string({ length: 6, pool: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' });

            // 1.1 generate user token
            let token = await tokenManager.generateToken(trans, reqUser, data, { user: "__super_user__" });

            // 1.2 setup user access
            await setupAccessService.process(trans, { tokenCode: token.code, password: oldPassword }, { user: "__super_user__" });

            // 1.3 update the user profiles
            await userDao.updateByFilter(trans, { profiles }, { code: reqUser, wrongAttempts: 1 }, { include: ['profiles'], user: "__super_user__" });

            // 1.4 changePAssword service request
            let req = { reqRef, reqUser, oldPassword: "", newPassword };

            // 2. execute test
            let changePwdSrvRsp = await changePasswordService.process(trans, req, { user: "__super_user__" })

            // 3. check result 
            // 3.1 check return response

            assert.ok(changePwdSrvRsp);
            assert.strictEqual(changePwdSrvRsp.rspCode, 'D');
            assert.strictEqual(changePwdSrvRsp.rspReason, 'ERR_ACCESS_SRV__CHNG_PWD_SRV__INV_OLD_PWD');


            // 3.2 check user WrongAttempt and status

            let userResult = await userDao.findByFilter(trans, { code: reqUser }, { user: "__super_user__" });
            assert.strictEqual(userResult.length, 1)
            assert.strictEqual(userResult[0].status, 'A')
            assert.strictEqual(userResult[0].wrongAttempts, 0)

            // 3.3 setupAccess should not be created
            let setupAccessResult = await setupAccessDao.findAll(trans, { user: "__super_user__" });
            assert.strictEqual(setupAccessResult.length, 0)

            // 3.4 check that request has been logged
            let logResult = await changePasswordLogDao.findAll(trans, { user: "__super_user__" });
            
            assert.ok(logResult);
            assert.strictEqual(logResult.length, 1);
            assert.strictEqual(logResult[0].rspCode, 'D');
            assert.strictEqual(logResult[0].rspReason, 'ERR_ACCESS_SRV__CHNG_PWD_SRV__INV_OLD_PWD');
            assert.ok(logResult[0].oldPassword !== oldPassword);
            assert.strictEqual(logResult[0].oldPassword, null);
            assert.ok(logResult[0].newPassword !== newPassword);
            assert.strictEqual(logResult[0].newPassword.length, 60);

        });


    it('\n' +
        '       | Conditions: \n' +
        '       | - userCode is valid \n' +
        '       | - oldPassword is valid \n' +
        '       | - newPassword is empty string \n' +
        '       | - user.wrongAttempts is 0  \n' +
        '       | - user.status is A(ctive)  \n' +
        '       | - maxWrongAttempts is 3  \n' +
        '       | Expected :  \n' +
        '       | -> should return rspCode D(eclined).  \n' +
        '', async () => {

            // 1. prepare test-data 
            let reqRef = chance.string({ length: 10 });
            let reqUser = chance.string({ length: 10 });
            let data = { setupMode: 'Automatic' };
            let oldPassword = chance.string({ length: 6, pool: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' });
            let newPassword = chance.string({ length: 6, pool: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' });

            // 1.1 generate user token
            let token = await tokenManager.generateToken(trans, reqUser, data, { user: "__super_user__" });

            // 1.2 setup user access
            await setupAccessService.process(trans, { tokenCode: token.code, password: oldPassword }, { user: "__super_user__" });

            // 1.3 update the user profiles
            await userDao.updateByFilter(trans, { profiles }, { code: reqUser }, { include: ['profiles'], user: "__super_user__" });

            // 1.4 changePAssword service request
            let req = { reqRef, reqUser, oldPassword: oldPassword, newPassword: "" };

            // 2. execute test
            let changePwdSrvRsp = await changePasswordService.process(trans, req, { user: "__super_user__" })

            // 3. check result 
            // 3.1 check return response

            assert.ok(changePwdSrvRsp);
            assert.strictEqual(changePwdSrvRsp.rspCode, 'D');
            assert.strictEqual(changePwdSrvRsp.rspReason, 'ERR_ACCESS_SRV__CHNG_PWD_SRV__INV_NEW_PWD');


            // 3.2 check user WrongAttempt and status

            let userResult = await userDao.findByFilter(trans, { code: reqUser }, { user: "__super_user__" });
            assert.strictEqual(userResult.length, 1)
            assert.strictEqual(userResult[0].status, 'A')
            assert.strictEqual(userResult[0].wrongAttempts, 0)

            // 3.3 setupAccess should not be created
            let setupAccessResult = await setupAccessDao.findAll(trans, { user: "__super_user__" });
            assert.strictEqual(setupAccessResult.length, 0)

            // 3.4 check that request has been logged
            let logResult = await changePasswordLogDao.findAll(trans, { user: "__super_user__" });

            assert.ok(logResult);
            assert.strictEqual(logResult.length, 1);
            assert.strictEqual(logResult[0].rspCode, 'D');
            assert.strictEqual(logResult[0].rspReason, 'ERR_ACCESS_SRV__CHNG_PWD_SRV__INV_NEW_PWD');
            assert.ok(logResult[0].oldPassword !== oldPassword);
            assert.strictEqual(logResult[0].oldPassword.length, 60);
            assert.ok(logResult[0].newPassword !== newPassword);
            assert.strictEqual(logResult[0].newPassword, null);

        });

    it('\n' +
        '       | Conditions: \n' +
        '       | - userCode is valid \n' +
        '       | - oldPassword is wrong \n' +
        '       | - newPassword is valid \n' +
        '       | - user.wrongAttempts is 0  \n' +
        '       | - user.status is A(ctive)  \n' +
        '       | - maxWrongAttempts is 3  \n' +
        '       | Expected :  \n' +
        '       | -> should return rspCode D(eclined).  \n' +
        '       | -> should set wrongAttempts to 1.  \n' +
        '', async () => {

            // 1. prepare test-data 
            let reqRef = chance.string({ length: 10 });
            let reqUser = chance.string({ length: 10 });
            let data = { setupMode: 'Automatic' };
            let oldPassword = chance.string({ length: 6, pool: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' });
            let newPassword = chance.string({ length: 6, pool: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' });

            // 1.1 generate user token
            let token = await tokenManager.generateToken(trans, reqUser, data, { user: "__super_user__" });

            // 1.2 setup user access
            await setupAccessService.process(trans, { tokenCode: token.code, password: oldPassword }, { user: "__super_user__" });

            // 1.3 update the user profiles
            await userDao.updateByFilter(trans, { profiles }, { code: reqUser }, { include: ['profiles'], user: "__super_user__" });

            // 1.4 changePAssword service request
            let req = { reqRef, reqUser, oldPassword: "wrong", newPassword: newPassword };

            // 2. execute test
            let changePwdSrvRsp = await changePasswordService.process(trans, req, { user: "__super_user__" })

            // 3. check result 
            // 3.1 check return response

            assert.ok(changePwdSrvRsp);
            assert.strictEqual(changePwdSrvRsp.rspCode, 'D');
            assert.strictEqual(changePwdSrvRsp.rspReason, 'ERR_ACCESS_SRV__CHNG_PWD_SRV__ACCESS');


            // 3.2 check user WrongAttempt and status

            let userResult = await userDao.findByFilter(trans, { code: reqUser }, { user: "__super_user__" });
            assert.strictEqual(userResult.length, 1)
            assert.strictEqual(userResult[0].status, 'A')
            assert.strictEqual(userResult[0].wrongAttempts, 1)

            // 3.3 setupAccess should not be created
            let setupAccessResult = await setupAccessDao.findAll(trans, { user: "__super_user__" });
            assert.strictEqual(setupAccessResult.length, 0)

            // 3.4 check that request has been logged
            let logResult = await changePasswordLogDao.findAll(trans, { user: "__super_user__" });

            assert.ok(logResult);
            assert.strictEqual(logResult.length, 1);
            assert.strictEqual(logResult[0].rspCode, 'D');
            assert.strictEqual(logResult[0].rspReason, 'ERR_ACCESS_SRV__CHNG_PWD_SRV__ACCESS');
            assert.ok(logResult[0].oldPassword !== oldPassword);
            assert.strictEqual(logResult[0].oldPassword.length, 60);
            assert.ok(logResult[0].newPassword !== newPassword);
            assert.strictEqual(logResult[0].newPassword.length, 60);

        });

    it('\n' +
        '       | Conditions: \n' +
        '       | - userCode is valid \n' +
        '       | - oldPassword is wrong \n' +
        '       | - newPassword is valid \n' +
        '       | - user.wrongAttempts is 2  \n' +
        '       | - user.status is A(ctive)  \n' +
        '       | - maxWrongAttempts is 3  \n' +
        '       | Expected :  \n' +
        '       | -> should return rspCode D(eclined).  \n' +
        '       | -> should set wrongAttempts to 3.  \n' +
        '       | -> should lock user.  \n' +
        '', async () => {

            // 1. prepare test-data 
            let reqRef = chance.string({ length: 10 });
            let reqUser = chance.string({ length: 10 });
            let data = { setupMode: 'Automatic' };
            let oldPassword = chance.string({ length: 6, pool: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' });
            let newPassword = chance.string({ length: 6, pool: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' });

            // 1.1 generate user token
            let token = await tokenManager.generateToken(trans, reqUser, data, { user: "__super_user__" });

            // 1.2 setup user access
            await setupAccessService.process(trans, { tokenCode: token.code, password: oldPassword }, { user: "__super_user__" });

            // 1.3 update the user profiles
            await userDao.updateByFilter(trans, { profiles, wrongAttempts: 2  }, { code: reqUser}, { include: ['profiles'], user: "__super_user__" });

            // 1.4 changePAssword service request
            let req = { reqRef, reqUser, oldPassword: "wrong", newPassword: newPassword };

            // 2. execute test
            let changePwdSrvRsp = await changePasswordService.process(trans, req, { user: "__super_user__" })

            // 3. check result 
            // 3.1 check return response

            assert.ok(changePwdSrvRsp);
            assert.strictEqual(changePwdSrvRsp.rspCode, 'D');
            assert.strictEqual(changePwdSrvRsp.rspReason, 'ERR_ACCESS_SRV__CHNG_PWD_SRV__ACCESS');


            // 3.2 check user WrongAttempt and status

            let userResult = await userDao.findByFilter(trans, { code: reqUser }, { user: "__super_user__" });
            assert.strictEqual(userResult.length, 1)
            assert.strictEqual(userResult[0].status, 'L')
            assert.strictEqual(userResult[0].wrongAttempts, 3)

            // 3.3 setupAccess should not be created
            let setupAccessResult = await setupAccessDao.findAll(trans, { user: "__super_user__" });
            assert.strictEqual(setupAccessResult.length, 0)

            // 3.4 check that request has been logged
            let logResult = await changePasswordLogDao.findAll(trans, { user: "__super_user__" });

            assert.ok(logResult);
            assert.strictEqual(logResult.length, 1);
            assert.strictEqual(logResult[0].rspCode, 'D');
            assert.strictEqual(logResult[0].rspReason, 'ERR_ACCESS_SRV__CHNG_PWD_SRV__ACCESS');
            assert.ok(logResult[0].oldPassword !== oldPassword);
            assert.strictEqual(logResult[0].oldPassword.length, 60);
            assert.ok(logResult[0].newPassword !== newPassword);
            assert.strictEqual(logResult[0].newPassword.length, 60);

        });


    it('\n' +
        '       | Conditions: \n' +
        '       | - userCode is valid \n' +
        '       | - oldPassword is wrong \n' +
        '       | - newPassword is valid \n' +
        '       | - user.wrongAttempts is 3  \n' +
        '       | - user.status is A(ctive)  \n' +
        '       | - maxWrongAttempts is 3  \n' +
        '       | Expected :  \n' +
        '       | -> should return rspCode D(eclined).  \n' +
        '       | -> should set wrongAttempts to 3.  \n' +
        '       | -> should lock user.  \n' +
        '', async () => {

            // 1. prepare test-data 
            let reqRef = chance.string({ length: 10 });
            let reqUser = chance.string({ length: 10 });
            let data = { setupMode: 'Automatic' };
            let oldPassword = chance.string({ length: 6, pool: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' });
            let newPassword = chance.string({ length: 6, pool: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' });

            // 1.1 generate user token
            let token = await tokenManager.generateToken(trans, reqUser, data, { user: "__super_user__" });

            // 1.2 setup user access
            await setupAccessService.process(trans, { tokenCode: token.code, password: oldPassword }, { user: "__super_user__" });

            // 1.3 update the user profiles
            await userDao.updateByFilter(trans, { profiles, wrongAttempts: 3 }, { code: reqUser }, { include: ['profiles'], user: "__super_user__" });

            // 1.4 changePAssword service request
            let req = { reqRef, reqUser, oldPassword: "wrong", newPassword: newPassword };

            // 2. execute test
            let changePwdSrvRsp = await changePasswordService.process(trans, req, { user: "__super_user__" })

            // 3. check result 
            // 3.1 check return response

            assert.ok(changePwdSrvRsp);
            assert.strictEqual(changePwdSrvRsp.rspCode, 'D');
            assert.strictEqual(changePwdSrvRsp.rspReason, 'ERR_ACCESS_SRV__CHNG_PWD_SRV__ACCESS');


            // 3.2 check user WrongAttempt and status

            let userResult = await userDao.findByFilter(trans, { code: reqUser }, { user: "__super_user__" });
            assert.strictEqual(userResult.length, 1)
            assert.strictEqual(userResult[0].status, 'A')
            assert.strictEqual(userResult[0].wrongAttempts, 3)

            // 3.3 setupAccess should not be created
            let setupAccessResult = await setupAccessDao.findAll(trans, { user: "__super_user__" });
            assert.strictEqual(setupAccessResult.length, 0)

            // 3.4 check that request has been logged
            let logResult = await changePasswordLogDao.findAll(trans, { user: "__super_user__" });

            assert.ok(logResult);
            assert.strictEqual(logResult.length, 1);
            assert.strictEqual(logResult[0].rspCode, 'D');
            assert.strictEqual(logResult[0].rspReason, 'ERR_ACCESS_SRV__CHNG_PWD_SRV__ACCESS');
            assert.ok(logResult[0].oldPassword !== oldPassword);
            assert.strictEqual(logResult[0].oldPassword.length, 60);
            assert.ok(logResult[0].newPassword !== newPassword);
            assert.strictEqual(logResult[0].newPassword.length, 60);

        });

    it('\n' +
        '       | Conditions: \n' +
        '       | - userCode is valid \n' +
        '       | - oldPassword is wrong \n' +
        '       | - newPassword is valid \n' +
        '       | - user.wrongAttempts is 0  \n' +
        '       | - user.status is L(ocked)  \n' +
        '       | - maxWrongAttempts is 3  \n' +
        '       | Expected :  \n' +
        '       | -> should return rspCode D(eclined).  \n' +
        '       | -> should set wrongAttempts to 0.  \n' +
        '       | -> should lock user.  \n' +
        '', async () => {

            // 1. prepare test-data 
            let reqRef = chance.string({ length: 10 });
            let reqUser = chance.string({ length: 10 });
            let data = { setupMode: 'Automatic' };
            let oldPassword = chance.string({ length: 6, pool: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' });
            let newPassword = chance.string({ length: 6, pool: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' });

            // 1.1 generate user token
            let token = await tokenManager.generateToken(trans, reqUser, data, { user: "__super_user__" });

            // 1.2 setup user access
            await setupAccessService.process(trans, { tokenCode: token.code, password: oldPassword }, { user: "__super_user__" });

            // 1.3 update the user profiles
            await userDao.updateByFilter(trans, { profiles, status: 'L' }, { code: reqUser }, { include: ['profiles'], user: "__super_user__" });

            // 1.4 changePAssword service request
            let req = { reqRef, reqUser, oldPassword: "wrong", newPassword: newPassword };

            // 2. execute test
            let changePwdSrvRsp = await changePasswordService.process(trans, req, { user: "__super_user__" })

            // 3. check result 
            // 3.1 check return response

            assert.ok(changePwdSrvRsp);
            assert.strictEqual(changePwdSrvRsp.rspCode, 'D');
            assert.strictEqual(changePwdSrvRsp.rspReason, 'ERR_ACCESS_SRV__CHNG_PWD_SRV__ACCESS');


            // 3.2 check user WrongAttempt and status

            let userResult = await userDao.findByFilter(trans, { code: reqUser }, { user: "__super_user__" });
            assert.strictEqual(userResult.length, 1)
            assert.strictEqual(userResult[0].status, 'L')
            assert.strictEqual(userResult[0].wrongAttempts, 0)

            // 3.3 setupAccess should not be created
            let setupAccessResult = await setupAccessDao.findAll(trans, { user: "__super_user__" });
            assert.strictEqual(setupAccessResult.length, 0)

            // 3.4 check that request has been logged
            let logResult = await changePasswordLogDao.findAll(trans, { user: "__super_user__" });

            assert.ok(logResult);
            assert.strictEqual(logResult.length, 1);
            assert.strictEqual(logResult[0].rspCode, 'D');
            assert.strictEqual(logResult[0].rspReason, 'ERR_ACCESS_SRV__CHNG_PWD_SRV__ACCESS');
            assert.ok(logResult[0].oldPassword !== oldPassword);
            assert.strictEqual(logResult[0].oldPassword.length, 60);
            assert.ok(logResult[0].newPassword !== newPassword);
            assert.strictEqual(logResult[0].newPassword.length, 60);

        });


    it('\n' +
        '       | Conditions: \n' +
        '       | - userCode is valid \n' +
        '       | - oldPassword is wrong \n' +
        '       | - newPassword is valid \n' +
        '       | - user.wrongAttempts is 0  \n' +
        '       | - user.status is I(nactive)  \n' +
        '       | - maxWrongAttempts is 3  \n' +
        '       | Expected :  \n' +
        '       | -> should return rspCode I(nactive).  \n' +
        '       | -> should set wrongAttempts to 0.  \n' +
        '       | -> should keep the user inactive.  \n' +
        '', async () => {

            // 1. prepare test-data 
            let reqRef = chance.string({ length: 10 });
            let reqUser = chance.string({ length: 10 });
            let data = { setupMode: 'Automatic' };
            let oldPassword = chance.string({ length: 6, pool: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' });
            let newPassword = chance.string({ length: 6, pool: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' });

            // 1.1 generate user token
            let token = await tokenManager.generateToken(trans, reqUser, data, { user: "__super_user__" });

            // 1.2 setup user access
            await setupAccessService.process(trans, { tokenCode: token.code, password: oldPassword }, { user: "__super_user__" });

            // 1.3 update the user profiles
            await userDao.updateByFilter(trans, { profiles, status: 'I' }, { code: reqUser }, { include: ['profiles'], user: "__super_user__" });

            // 1.4 changePAssword service request
            let req = { reqRef, reqUser, oldPassword: "wrong", newPassword: newPassword };

            // 2. execute test
            let changePwdSrvRsp = await changePasswordService.process(trans, req, { user: "__super_user__" })

            // 3. check result 
            // 3.1 check return response

            assert.ok(changePwdSrvRsp);
            assert.strictEqual(changePwdSrvRsp.rspCode, 'D');
            assert.strictEqual(changePwdSrvRsp.rspReason, 'ERR_ACCESS_SRV__CHNG_PWD_SRV__ACCESS');


            // 3.2 check user WrongAttempt and status

            let userResult = await userDao.findByFilter(trans, { code: reqUser }, { user: "__super_user__" });
            assert.strictEqual(userResult.length, 1)
            assert.strictEqual(userResult[0].status, 'I')
            assert.strictEqual(userResult[0].wrongAttempts, 0)

            // 3.3 setupAccess should not be created
            let setupAccessResult = await setupAccessDao.findAll(trans, { user: "__super_user__" });
            assert.strictEqual(setupAccessResult.length, 0)

            // 3.4 check that request has been logged
            let logResult = await changePasswordLogDao.findAll(trans, { user: "__super_user__" });

            assert.ok(logResult);
            assert.strictEqual(logResult.length, 1);
            assert.strictEqual(logResult[0].rspCode, 'D');
            assert.strictEqual(logResult[0].rspReason, 'ERR_ACCESS_SRV__CHNG_PWD_SRV__ACCESS');
            assert.ok(logResult[0].oldPassword !== oldPassword);
            assert.strictEqual(logResult[0].oldPassword.length, 60);
            assert.ok(logResult[0].newPassword !== newPassword);
            assert.strictEqual(logResult[0].newPassword.length, 60);

        });
});
