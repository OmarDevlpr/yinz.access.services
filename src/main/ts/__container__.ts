import Container from "@yinz/container/ts/Container";
import { Logger } from "@yinz/commons";
import LoginService from "./LoginService";
import AccessManager from "@yinz/access.data/ts/AccessManager";
import { TypeormDao } from "@yinz/commons.data";
import User from "@yinz/access.data/models/User";
import LoginLog from "@yinz/access.data/models/LoginLog";
import ChangePasswordLog from "@yinz/access.data/models/ChangePasswordLog";
import SetupAccessService from "./SetupAccessService";
import SetupAccess from "@yinz/access.data/models/SetupAccess";
import SetupAccessLog from "@yinz/access.data/models/SetupAccessLog";
import ResetAccessLog from "@yinz/access.data/models/ResetAccessLog";
import Profile from "@yinz/access.data/models/Profile";
import TokenManager from "@yinz/token.services/ts/TokenManager";
import ResetAccessService from "./ResetAccessService";
import ChangePasswordService from "./ChangePasswordService";
import PrepareSetupAccessService from "./PrepareSetupAccessService";
import PrepareResetAccessService from "./PrepareResetAccessService";
import NotifManager from "@yinz/notif.services/ts/NotifManager";



const registerBeans = (container: Container) => {
    
    let loginService = new LoginService({ 
        logger          : container.getBean<Logger>('logger'),
        accessManager   : container.getBean<AccessManager>('accessManager'),
        userDao         : container.getBean<TypeormDao<User>>("userDao"),
        logDao          : container.getBean<TypeormDao<LoginLog>>("loginLogDao"),
     });

     let changePasswordService = new ChangePasswordService({
         logger: container.getBean<Logger>('logger'),
         accessManager: container.getBean<AccessManager>('accessManager'),
         userDao: container.getBean<TypeormDao<User>>("userDao"),
         logDao: container.getBean<TypeormDao<ChangePasswordLog>>("changePasswordLogDao"),
     })

     let setupAccessService = new SetupAccessService({
         logger: container.getBean<Logger>('logger'),
         accessManager: container.getBean<AccessManager>('accessManager'),
         userDao: container.getBean<TypeormDao<User>>("userDao"),
         profileDao: container.getBean<TypeormDao<Profile>>("profileDao"),
         setupAccessDao: container.getBean<TypeormDao<SetupAccess>>("setupAccessDao"),
         logDao: container.getBean<TypeormDao<SetupAccessLog>>("setupAccessLogDao"),
         tokenManager: container.getBean<TokenManager>("tokenManager")
     });

    let resetAccessService = new ResetAccessService({
        logger: container.getBean<Logger>('logger'),
        accessManager: container.getBean<AccessManager>('accessManager'),
        userDao: container.getBean<TypeormDao<User>>("userDao"),                
        logDao: container.getBean<TypeormDao<ResetAccessLog>>("resetAccessLogDao"),
        tokenManager: container.getBean<TokenManager>("tokenManager")
    });

    let prepareResetAccessService = new PrepareResetAccessService({
        logger: container.getBean<Logger>('logger'),
        accessManager: container.getBean<AccessManager>('accessManager'),
        userDao: container.getBean<TypeormDao<User>>("userDao"),
        logDao: container.getBean<TypeormDao<ResetAccessLog>>("prepareResetAccessLogDao"),
        tokenManager: container.getBean<TokenManager>("tokenManager"),
        notifManager: container.getBean<NotifManager>("notifManager")
    })



    container.setBean('loginService', loginService);
    container.setBean('setupAccessService', setupAccessService);
    container.setBean('resetAccessService', resetAccessService);
    container.setBean('prepareResetAccessService', prepareResetAccessService);
    container.setBean('changePasswordService', changePasswordService);

};

const registerClazzes = (container: Container) => {
    
    container.setClazz('LoginService', LoginService);
    container.setClazz('SetupAccessService', SetupAccessService);
    container.setClazz('ResetAccessService', ResetAccessService);
    container.setClazz('ChangePasswordService', ChangePasswordService);
    container.setClazz('PrepareSetupAccessService', PrepareSetupAccessService);
    container.setClazz('PrepareResetAccessService', PrepareResetAccessService);

};


export {
    registerBeans,
    registerClazzes
};