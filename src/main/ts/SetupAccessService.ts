import ServiceTemplate, { DefaultServiceRsp, DefaultServiceReq, ServiceTemplateOptions } from "@yinz/commons.services/ts/ServiceTemplate";
import { TypeormDao } from "@yinz/commons.data";
import User from "@yinz/access.data/models/User";
import Profile from "@yinz/access.data/models/Profile";
import SetupAccess from "@yinz/access.data/models/SetupAccess";
import TokenManager from "@yinz/token.services/ts/TokenManager";
import { Exception, YinzOptions, PasswordUtils } from "@yinz/commons";
import { YinzTransaction, YinzConnection } from "@yinz/commons.data/ts/DataSource";
import Token from "@yinz/token.data/models/Token";
import * as _ from "lodash"
import LogModel from "@yinz/commons.data/ts/LogModel";


export interface SetupAccessServiceReq extends DefaultServiceReq {
    tokenCode: string;
    password?: string;
    token?: Token;
    passwordDigest?: string;
}
export interface SetupAccessServiceRsp extends DefaultServiceRsp {

}

export interface SetupAccessServiceOptions extends ServiceTemplateOptions {
    userDao: TypeormDao<User>;
    profileDao: TypeormDao<Profile>;
    setupAccessDao: TypeormDao<SetupAccess>;    
    tokenManager: TokenManager;
};

export default class SetupAccessService extends ServiceTemplate<SetupAccessServiceReq, SetupAccessServiceRsp> {

    private _userDao: TypeormDao<User>;
    private _profileDao: TypeormDao<Profile>;
    private _setupAccessDao: TypeormDao<SetupAccess>;
    private _tokenManager: TokenManager;
        

    constructor(options: SetupAccessServiceOptions) {
        options.serviceName = "SetupAccessService";
        options.logReqRsp = true;

        super(options);

        this._accessManager = options.accessManager;
        this._userDao = options.userDao;
        this._profileDao = options.profileDao;
        this._tokenManager = options.tokenManager
        this._setupAccessDao = options.setupAccessDao;

        this._logger = options.logger

    }

    protected async assertBusinessRules(handler: YinzConnection | YinzTransaction, req: SetupAccessServiceReq, options?: YinzOptions &{ context?: any}): Promise<void> {

        options = options || {};
        options.context = options.context || {};

        // 1. call parent method
        super.assertBusinessRules(handler, req , options);

        // 2. at this stage we have to add the reqUser to the request
        req.reqUser = options.context.user
        
    }

    protected valReqRemFlds(req: SetupAccessServiceReq, options?: YinzOptions): void {

        super.valReqRemFlds(req, options);

        // make sure that tokenCode and password are good enough to work with
        if (!_.isString(req.tokenCode) || _.isEmpty(req.tokenCode)) {
            req.tokenCode = null as any;
            throw new Exception('ERR_ACCESS_SRV__SETUP_ACCESS_SRV__INV_TOKEN', {
                message: 'The supplied tokenCode ' + req.tokenCode + ' is invalid! It should be a none-empty string.'
            });
        }
        if ((!_.isString(req.password) || _.isEmpty(req.password)) && (!_.isString(req.passwordDigest) || _.isEmpty(req.passwordDigest))) {
            req.password = null as any;
            throw new Exception('ERR_ACCESS_SRV__SETUP_ACCESS_SRV__INV_PWD', {
                message: 'The supplied password ' + req.password + '/' + req.passwordDigest + '' + ' is invalid! It should be a none-empty string.'
            });
        }
        
    }

    private async fetchToken(handler: YinzConnection | YinzTransaction, req: SetupAccessServiceReq,  tokenCode: string, options?: YinzOptions): Promise<Token> {

        let token = await this._tokenManager.lookupToken(handler, req.tokenCode, null as any, options);
        req.token = token;
        // req.reqUser = token.owner;
    
        return token;
    }

    protected async executeService(handler: YinzConnection | YinzTransaction, req: SetupAccessServiceReq, options?: YinzOptions): Promise<SetupAccessServiceRsp> {

        let tokenCode = req.tokenCode;
        let password  = req.password;
        
        req.token = req.token || await this.fetchToken(handler, req, tokenCode, options);

        // by default, users don't have any profile
        let profileIds = req.token.data && req.token.data["profileIds"] || [];

        if (profileIds.length === 0) {
            this._logger.warn('No profiles assigned to user %s!', req.reqUser);
        }

        // populate profiles if any
        let profiles = new Array<Profile>(profileIds.length);

        for (let index = 0; index < profileIds.length; index++) {
            profiles[index] = await this._profileDao.findById(handler, profileIds[index], options)
        }


        // setup mode 

        let setupMode: "Manual" | "Automatic" = req.token.data && req.token.data["setupMode"] || "Manual"
        let ownerId   = req.token.data && req.token.data["ownerId"]
        let ownerModelName   = req.token.data && req.token.data["ownerModelName"]


        // warning in case neither Automatic nor Manual
        if (['Automatic', 'Manual'].indexOf(setupMode) === -1) {
            this._logger.warn('SetupMode is set to "%s". It should be either "Automatic" or "Manual"! It will be forced to "Manual".', setupMode);
            setupMode = 'Manual';
        }
        
        if ( setupMode === "Automatic") {

            let user = new User();
            user.code = req.reqUser || req.token.owner;
            user.password = password as string;
            user.ownerId = ownerId;
            user.ownerModelName = ownerModelName;
            user.wrongAttempts = 0;
            user.status = "A";
            
            if (profiles.length > 0) {
                user.profiles = profiles;
            }

            await this._userDao.create(handler, user, {...options, include: ['profiles'] } );
            
        } else {

            let setupAccess = new SetupAccess();
            setupAccess.code = req.reqUser || req.token.owner;
            setupAccess.password = password as string;
            setupAccess.ownerId = ownerId;
            setupAccess.ownerModelName = ownerModelName;
            setupAccess.status = "W";

            if (profiles.length > 0) {
                setupAccess.profiles = profiles;
            }
                
            
            await this._setupAccessDao.create(handler, setupAccess, { ...options, include: ['profiles'] });
        }

        return {};
    }

    protected async logReq(handler: YinzConnection | YinzTransaction, req: SetupAccessServiceReq, options?: YinzOptions & {context?: any}): Promise<LogModel> {

        options = options || {};
        options.context = options.context || {};

        this.valReqRemFlds(req, options);

        // in case the user has provided a passwrod digest, hash it and remove it from the request before it gets logged.

        req.token = req.token || await this.fetchToken(handler, req, req.tokenCode, options);
        let reqUser = req.token.owner;        

        let hash = _.isString(req.passwordDigest) && !_.isEmpty(req.passwordDigest) ?
            req.passwordDigest :
            (_.isString(reqUser) && !_.isEmpty(reqUser) && _.isString(req.password) && !_.isEmpty(req.password) ?
                await PasswordUtils.hashPassword(req.password, reqUser) :
                null) as string;

        req.password = hash;

        // req.passwordDigest is not in the model.
        if (req.passwordDigest) {
            delete req.passwordDigest;
        }


        let logReqClone = {...req};
        delete logReqClone.token;

        // get hold of user
        options.context.user = reqUser;

        return super.logReq(handler, logReqClone, options);

    }
    
}