import ServiceTemplate, { DefaultServiceReq, DefaultServiceRsp, ServiceTemplateOptions } from "@yinz/commons.services/ts/ServiceTemplate";
import { YinzOptions, Exception, PasswordUtils } from "@yinz/commons";
import * as _ from "lodash"
import LogModel from "@yinz/commons.data/ts/LogModel";
import Token from "@yinz/token.data/models/Token";
import { YinzConnection, YinzTransaction } from "@yinz/commons.data/ts/DataSource";
import User from "@yinz/access.data/models/User";
import TokenManager from "@yinz/token.services/ts/TokenManager";
import { TypeormDao } from "@yinz/commons.data";


export interface ResetAccessServiceReq extends DefaultServiceReq {
    tokenCode: string;
    password?: string;
    passwordDigest?: string;
    token?: Token;
}

export interface ResetAccessServiceRsp extends DefaultServiceRsp {

}

export interface ResetAccessOptions extends ServiceTemplateOptions {
    userDao: TypeormDao<User>;
    tokenManager: TokenManager;
}

export default class ResetAccessService extends ServiceTemplate<ResetAccessServiceReq, ResetAccessServiceRsp> {


    private _userDao: TypeormDao<User>;
    private _tokenManager: TokenManager;

    constructor(options: ResetAccessOptions) {

        options.serviceName = "ResetAccessService";
        options.logReqRsp = true;

        super(options)

        this._accessManager = options.accessManager;
        this._userDao = options.userDao;
        this._tokenManager = options.tokenManager
        this._logger = options.logger

    }

    protected valReqRemFlds(req: ResetAccessServiceReq, options?: YinzOptions): void {

        super.valReqRemFlds(req, options);

        // make sure that tokenCode and password are good enough to work with
        if (!_.isString(req.tokenCode) || _.isEmpty(req.tokenCode)) {
            req.tokenCode = null as any;
            throw new Exception('ERR_ACCESS_SRV__RESET_ACCESS_SRV__INV_TOKEN', {
                message: 'The supplied tokenCode ' + req.tokenCode + ' is invalid! It should be a none-empty string.'
            });
        }
        if ((!_.isString(req.password) || _.isEmpty(req.password)) && (!_.isString(req.passwordDigest) || _.isEmpty(req.passwordDigest))) {
            req.password = null as any;
            throw new Exception('ERR_ACCESS_SRV__RESET_ACCESS_SRV__INV_PWD', {
                message: 'The supplied password ' + req.password + '/' + req.passwordDigest + '' + ' is invalid! It should be a none-empty string.'
            });
        }

    }


    private async fetchToken(handler: YinzConnection | YinzTransaction, req: ResetAccessServiceReq, tokenCode: string, options?: YinzOptions): Promise<Token> {

        let token = await this._tokenManager.lookupToken(handler, req.tokenCode, null as any, options);
        req.token = token;
        req.reqUser = token.owner;

        return token;
    }

    protected async executeService(handler: YinzConnection | YinzTransaction, req: ResetAccessServiceReq, options?: YinzOptions): Promise<ResetAccessServiceRsp> {

        let user = new User();
        user.password = req.password as string;
        user.status   = "A";
        user.wrongAttempts = 0;
        
        await this._userDao.updateByFilter(handler, user, {code: req.reqUser}, options)

        // TODO invalidate all existing sessions

        return {};
    }


    protected async logReq(handler: YinzConnection | YinzTransaction, req: ResetAccessServiceReq, options?: YinzOptions): Promise<LogModel> {
        
        this.valReqRemFlds(req, options);

        // in case the user has provided a passwrod digest, hash it and remove it from the request before it gets logged.

        req.token = req.token || await this.fetchToken(handler, req, req.tokenCode, options);
        req.reqUser = req.token.owner;

        let hash = _.isString(req.passwordDigest) && !_.isEmpty(req.passwordDigest) ?
            req.passwordDigest :
            (_.isString(req.reqUser) && !_.isEmpty(req.reqUser) && _.isString(req.password) && !_.isEmpty(req.password) ?
                await PasswordUtils.hashPassword(req.password, req.reqUser) :
                null) as string;

        req.password = hash;

        // req.passwordDigest is not in the model.
        if (req.passwordDigest) {
            delete req.passwordDigest;
        }


        let logReqClone = { ...req };
        delete logReqClone.token;
        
    
        return super.logReq(handler, logReqClone, options);
        

    }

}