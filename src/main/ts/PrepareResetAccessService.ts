import ServiceTemplate, { DefaultServiceReq, DefaultServiceRsp, ServiceTemplateOptions } from "@yinz/commons.services/ts/ServiceTemplate";
import TokenManager from "@yinz/token.services/ts/TokenManager";
import { TypeormDao } from "@yinz/commons.data";
import User from "@yinz/access.data/models/User";
import { YinzOptions, Asserts, Exception } from "@yinz/commons";
import { YinzConnection, YinzTransaction } from "@yinz/commons.data/ts/DataSource";
import Token from "@yinz/token.data/models/Token";
import Notif from "@yinz/notif.data/models/Notif";
import NotifManager from "@yinz/notif.services/ts/NotifManager";


export interface PrepareResetAccessServiceReq extends DefaultServiceReq {    
    resetAccessUrl?: string;
    reqLoc?: string;
    userCode: string;
}

export interface PrepareResetAccessServiceRsp extends DefaultServiceRsp {
}

export interface PrepareResetAccessServiceOptions extends ServiceTemplateOptions {
    notifManager: NotifManager;
    tokenManager: TokenManager;
    userDao: TypeormDao<User>    
}

export default class PrepareResetAccessService extends ServiceTemplate<PrepareResetAccessServiceReq, PrepareResetAccessServiceRsp> {

    private _tokenManager: TokenManager
    private _notifManager: NotifManager;
    private _userDao: TypeormDao<User>;
    
    

    constructor(options: PrepareResetAccessServiceOptions) {

        options.serviceName = "PrepareResetAccessService";
        options.logReqRsp = true;

        super(options);

        this._tokenManager = options.tokenManager;
        this._notifManager = options.notifManager;
        this._userDao = options.userDao;
                
    }

    protected valReqRemFlds(req: PrepareResetAccessServiceReq, options?: YinzOptions) {

        // call parent method
        super.valReqRemFlds(req, options);

        Asserts.isNonEmptyString(req.userCode, 'ERR_ACCESS_SRV__PRP_RESET_ACCESS_SRV__INV_USER_CODE', {
            message: 'The supplied userCode ' + null + ' is invalid! It should be a none-empty string.'
        })

    }


    protected async executeService(handler: YinzConnection | YinzTransaction, req: PrepareResetAccessServiceReq, options?: YinzOptions): Promise<PrepareResetAccessServiceRsp> {

        // find the user
        let user = await this._userDao.findSingleByFilter(handler, { code: req.userCode}, options);

        if ( !user ) {
            throw new Exception('ERR_ACCESSS_SRV__PRP_RST_ACCSS__USER_NOT_FOUND', {
                message: `no user with userCode ${req.userCode} was found !!`,                
                req
            })
        }

        // generate token
        let token = await this._tokenManager.generateToken(handler, user.code, null, options);

        // send notif to user
        await this.sendResetAccessInstructionsToUser(handler, req, user, token, options)

        return {};
    }

    private async sendResetAccessInstructionsToUser(handler: YinzConnection | YinzTransaction, req: PrepareResetAccessServiceReq, user: User, token: Token, options?: YinzOptions & { model?: any }): Promise<void> {

        this._logger.info(' user ---> ', JSON.stringify(user))

        let dest = user.code;
        let destType = dest.match(/\w+@\w+\.\w+/) ? 'E' : (dest.match(/0\d{6,15}/) ? 'S' : null);
        let userLang = user.languageCode || 'en';

        if ( destType === null) {
            throw new Exception('ERR_ACCESSS_SRV__SND_RST_ACCSS_INSTRCT__INV_DST_TYPE', {
                message: `could not determine destination type from userCode!`,
                userCode: user.code,
                req
            })
        }

        // notif
        let notif = new Notif();
        notif.ref = req.reqRef as string;
        notif.reason = "RESET_PASSWORD";
        notif.dest = dest;
        notif.destType = destType as 'E' | 'S';
        notif.languageCode = userLang;
        notif.subject = 'resetAccessInstructionsMessageSubject';
        notif.message = 'resetAccessInstructionsMessageBody';
        notif.readyOn = new Date();

        // notif templ model data   

        // default model
        let model = {
            notif: notif,
            token: token,
            user: user,
            req: req,
            origin: req.reqLoc,
            resetAccessUrl: req.resetAccessUrl,
        };

        if (options && options.model)
            model = { ...model, ...options.model }

        options = { ...options, model };

        await this._notifManager.post(handler, notif, options);

        return;

    }

}