import ServiceTemplate, { DefaultServiceReq, DefaultServiceRsp, ServiceTemplateOptions } from "@yinz/commons.services/ts/ServiceTemplate";
import { TypeormDao } from "@yinz/commons.data";
import User from "@yinz/access.data/models/User";
import { YinzOptions, Asserts, Exception, PasswordUtils } from "@yinz/commons";
import { YinzTransaction, YinzConnection } from "@yinz/commons.data/ts/DataSource";
import LogModel from "@yinz/commons.data/ts/LogModel";

import * as _ from "lodash";

export interface ChangePasswordServiceReq extends DefaultServiceReq {    
    oldPassword?: string;
    newPassword?: string;
    oldPasswordDigest?: string;
    newPasswordDigest?: string;
}

export interface ChangePasswordServiceRsp extends DefaultServiceRsp {    
}

export interface ChangePasswordServiceOptions extends ServiceTemplateOptions {
    userDao: TypeormDao<User>;
    params?: {
        maxWrongAttempt?: number;
    }
};


export default class ChangePasswordService extends ServiceTemplate<ChangePasswordServiceReq, ChangePasswordServiceRsp> {

    private _maxWrongAttempts: number;
    private _userDao: TypeormDao<User>;
    
    constructor(options: ChangePasswordServiceOptions) {

        options.serviceName = "ChangePasswordService";
        options.logReqRsp = true;

        super(options);

        this._accessManager = options.accessManager;
        this._userDao = options.userDao;        

        // params
        this._maxWrongAttempts = options.params && options.params.maxWrongAttempt || 3;

    }



    protected valReqRemFlds(req: ChangePasswordServiceReq, options?: YinzOptions): void {

        // call parent method
        super.valReqRemFlds(req, options);

        Asserts.isNonEmptyString(req.reqUser, 'ERR_ACCESS_SRV__CHNG_PWD_SRV__INV_USER_CODE', {
            message: 'The supplied reqUser ' + req.reqUser + ' is invalid! It should be a none-empty string.'
        })

        Asserts.isNonEmptyString(req.oldPassword, 'ERR_ACCESS_SRV__CHNG_PWD_SRV__INV_OLD_PWD', {
            message: 'The supplied password ' + req.oldPassword + ' is invalid! It should be a none-empty string.'
        })

        Asserts.isNonEmptyString(req.newPassword, 'ERR_ACCESS_SRV__CHNG_PWD_SRV__INV_NEW_PWD', {
            message: 'The supplied password ' + req.newPassword + ' is invalid! It should be a none-empty string.'
        })

    }     

    protected async executeService(handler: YinzConnection | YinzTransaction, req: ChangePasswordServiceReq, options?: YinzOptions): Promise<ChangePasswordServiceRsp> {

        let reqUser = req.reqUser;
        let oldPassword = req.oldPassword;
        let newPassword = req.newPassword;

        this._logger.trace('About to lookup user "%s"...', reqUser);
        let users: User[] = await this._userDao.findByFilter(handler, {code: reqUser}, options);
        

        // 1. make sure the user exists and one user only
        if (users.length > 1) {
            let errorMessage = 'There are ' + users.length + ' users having code ' + reqUser + '! user\'s code should be unique.';
            this._logger.error(errorMessage);
            throw new Exception('ERR_ACCESS_SRV__CHNG_PWD_SRV__TOO_MANY_USERS', {
                message: errorMessage,
                code: reqUser,
                users: users
            });
        }

        if (users.length === 0) {
            throw new Exception('ERR_ACCESS_SRV__CHNG_PWD_SRV__USER_NOT_FOUND', {
                message: 'User ' + reqUser + ' doesn\'t exist.',
                code: reqUser,
            });
        }

        let user = users[0];

        // 2. assert that user is active
        if (user.status === 'L') {
            throw new Exception('ERR_ACCESS_SRV__CHNG_PWD_SRV__USER_LOCKED', {
                message: 'The user ' + reqUser + ' is locked.',
                code: reqUser,
                user: user,
            });
        }
        if (user.status === 'I') {
            throw new Exception('ERR_ACCESS_SRV__CHNG_PWD_SRV__SER_INACTIVE', {
                message: 'The user ' + reqUser + ' is inactive.',
                code: reqUser,
                user: user,
            });
        }
        if (user.wrongAttempts >= this._maxWrongAttempts) {
            throw new Exception('ER_ACCESS_SRV__CHNG_PWD_SRV__WRONG_ATTEMPS_EXCEEDED', {
                message: 'The user ' + reqUser + ' is inactive.',
                code: reqUser,
                user: user,
            });
        }

        // 3. compare passwords

        if (oldPassword !== user.password) {

            // 3.1 update wrong attempts

            let updateFields: Partial<User> = { wrongAttempts: user.wrongAttempts + 1 };

            if (user.wrongAttempts + 1 >= this._maxWrongAttempts) {
                updateFields.status = "L";
            }

            await this._userDao.updateById(handler, updateFields, user.id, options)

            throw new Exception('ERR_ACCESS_SRV__CHNG_PWD_SRV__WRONG_PWD', {
                message: 'The old password supplied by ' + reqUser + ' is wrong.',
                code: reqUser,
                user: user,
            });

        }

        // 4. setup the new password and clear wrong attempts
        await this._userDao.updateById(handler, { password: newPassword, wrongAttempts: 0 }, user.id, options);        

        // 5. there is nothing to return
        return {};

    }

    protected async translateNegativeRsp(handler: YinzConnection | YinzTransaction, req: ChangePasswordServiceReq, rsp: ChangePasswordServiceRsp, options?: YinzOptions): Promise<ChangePasswordServiceRsp> {


        const ERR_CODES = ['INV_USER_CODE', 'INV_OLD_PWD', 'INV_NEW_PWD']

        if (rsp.rspReason && ERR_CODES.indexOf(rsp.rspReason.replace('ERR_ACCESS_SRV__CHNG_PWD_SRV__', '')) === -1) {
            rsp.rspReason = 'ERR_ACCESS_SRV__CHNG_PWD_SRV__ACCESS';
            rsp.rspMessage = 'Invalid username or password.';
        }

        return rsp;
    }

    protected async logReq(handler: YinzConnection | YinzTransaction, req: ChangePasswordServiceReq, options?: YinzOptions): Promise<LogModel> {

        // in case the user has provided a passwrod digest, hash it and remove it from the request before it gets logged.

        // 1. old password
        let oldHash = _.isString(req.oldPasswordDigest) && !_.isEmpty(req.oldPasswordDigest) ?
            req.oldPasswordDigest :
            (_.isString(req.reqUser) && !_.isEmpty(req.reqUser) && _.isString(req.oldPassword) && !_.isEmpty(req.oldPassword) ?
                await PasswordUtils.hashPassword(req.oldPassword, req.reqUser) :
                null) as string;

        req.oldPassword = oldHash;
    
        // 2. new password
        let newHash = _.isString(req.newPasswordDigest) && !_.isEmpty(req.newPasswordDigest) ?
            req.newPasswordDigest :
            (_.isString(req.reqUser) && !_.isEmpty(req.reqUser) && _.isString(req.newPassword) && !_.isEmpty(req.newPassword) ?
                await PasswordUtils.hashPassword(req.newPassword, req.reqUser) :
                null) as string;

        req.newPassword = newHash;

        // 3 req.oldPasswordDigest && req.newPasswordDigest is not in the model.
        if (req.oldPasswordDigest) {
            delete req.oldPasswordDigest;
        }
        if (req.newPasswordDigest) {
            delete req.newPasswordDigest;
        }

        return super.logReq(handler, req, options);

    }

}