import ServiceTemplate, { DefaultServiceReq, DefaultServiceRsp, ServiceTemplateOptions } from "@yinz/commons.services/ts/ServiceTemplate";
import TokenManager from "@yinz/token.services/ts/TokenManager";
import { TypeormDao } from "@yinz/commons.data";
import User from "@yinz/access.data/models/User";
import Profile from "@yinz/access.data/models/Profile";
import { YinzOptions, Asserts, Exception } from "@yinz/commons";
import { YinzConnection, YinzTransaction } from "@yinz/commons.data/ts/DataSource";
import Person from "@yinz/basic.data/models/Person";
import Token from "@yinz/token.data/models/Token";
import Notif from "@yinz/notif.data/models/Notif";
import NotifManager from "@yinz/notif.services/ts/NotifManager";

export interface AccessOwner {
    owner: Person
    ownerModelName?: string;
    ownerprofileCodes?: string[];
}

export interface PrepareSetupAccessServiceReq extends DefaultServiceReq {
    email?: string;
    phone?: string;
    firstName?: string;
    lastName?: string;
    fullName?: string;
    setupAccessUrl?: string;
    reqLoc?: string;
}

export interface PrepareSetupAccessServiceRsp extends DefaultServiceRsp {    
}

export interface PrepareSetupAccessServiceOptions extends ServiceTemplateOptions {
    notifManager: NotifManager;
    tokenManager: TokenManager;
    userDao: TypeormDao<User>
    profileDao: TypeormDao<Profile>
    notifSubjectTempl?: string;
    notifMessageTempl?: string;
}

export default class PrepareSetupAccessService<Req extends PrepareSetupAccessServiceReq, Rsp extends PrepareSetupAccessServiceRsp> extends ServiceTemplate
    <Req, Rsp> {

    private _tokenManager: TokenManager
    private _userDao: TypeormDao<User>;
    private _profileDao: TypeormDao<Profile>;

    private _notifSubjectTempl: string;
    private _notifMessageTempl: string;

    private _notifManager: NotifManager;


    constructor( options: PrepareSetupAccessServiceOptions) {

        options.serviceName = options.serviceName || "PrepareSetupAccessService";
        options.logReqRsp = true;

        super(options);

        this._tokenManager = options.tokenManager;
        this._userDao = options.userDao;
        this._profileDao = options.profileDao;

        this._notifSubjectTempl = options.notifSubjectTempl || 'setupAccessInstructionsMessageSubject';
        this._notifMessageTempl = options.notifMessageTempl || 'setupAccessInstructionsMessageBody';

        this._notifManager = options.notifManager;
        
    }

    protected valReqRemFlds(req: Req, options?: YinzOptions) {

        // call parent method
        super.valReqRemFlds(req, options);

        if ( req.hasOwnProperty('firstName') ) {
            Asserts.isNonEmptyString(req.firstName, "ERR_ACCESS_SRV__PREP_SETUP_ACCESS_SRV__INV_FIRST_NAME", {
                message: 'The supplied firstName ' + null + ' is invalid! It should be a none-empty string.'
            })
        }

        if ( req.hasOwnProperty('lastName') ) {
            Asserts.isNonEmptyString(req.lastName, "ERR_ACCESS_SRV__PREP_SETUP_ACCESS_SRV__INV_LAST_NAME", {
                message: 'The supplied lastName ' + null + ' is invalid! It should be a none-empty string.'
            })
        }

        if ( !req.hasOwnProperty('email') && !req.hasOwnProperty('phone') ) {
            throw new Exception('ERR_ACCESS_SRV__PREP_SETUP_ACCESS_SRV__MISS_EMAIL_AND_PHONE', {
                message: 'Both email and phone are missing in the request! You should supply one of the two and it should be a non-empty-string.',
                req: req
            });
        }

        if ( req.hasOwnProperty('email') ) {
            Asserts.isNonEmptyString(req.email, "ERR_ACCESS_SRV__PREP_SETUP_ACCESS_SRV__INV_EMAIL", {
                message: 'The supplied email ' + null + ' is invalid! It should be a none-empty string.'
            })
        }

        if ( req.hasOwnProperty('phone') ) {
            Asserts.isNonEmptyString(req.phone, "ERR_ACCESS_SRV__PREP_SETUP_ACCESS_SRV__INV_PHONE", {
                message: 'The supplied phone ' + null + ' is invalid! It should be a none-empty string.'
            })
        }

    }

    protected async assertBusinessRules(handler: YinzConnection| YinzTransaction, req: Req, options?: YinzOptions): Promise<void> {

        // call parent method
        await super.assertBusinessRules(handler, req, options);

        // make sure that email and phone are not use by another user

        let userCode = req.email || req.phone;

        let users = await this._userDao.findByFilter(handler, {code: userCode}, options);

        if ( users.length > 0 ) {
            if ( req.email ) {
                throw new Exception('ERR_ACCESS_SRV__PREP_SETUP_ACCESS_SRV__EMAIL_ALREADY_USED', {
                    message: 'The supplied email ' + req.email + ' is already used by another user! Please try another email.',
                    email: req.email,
                    req: req,
                });
            } else {
                throw new Exception('ERR_ACCESS_SRV__PREP_SETUP_ACCESS_SRV__PHONE_ALREADY_USED', {
                    message: 'The supplied phone ' + req.phone + ' is already used by another user! Please try another phone.',
                    phone: req.phone,
                    req: req,
                });
            }
        }

    }

    protected async executeService(handler: YinzConnection | YinzTransaction, req: Req, options?: YinzOptions): Promise<Rsp> {

        let rsp: Rsp = {} as Rsp;
        
        // create access owner
        let ownerInfo = await this.createAccessOwner(handler, req, rsp, options)
        let profileCodes = ownerInfo.ownerprofileCodes || [];

        // lookup profiles for access owner
        let profiles: Profile[] = [];

        if ( profileCodes.length > 1) {
            profiles = await this._profileDao.findByFilter(handler, { code: { op: "IN", val: profileCodes } }, options)
        } else if ( profileCodes.length === 1 ) {
            profiles = await this._profileDao.findByFilter(handler, { code: profileCodes[0] }, options)
        }
            
        let profileIds = profiles.map(profile => profile.id);


        // setup access, if required
        let tokenData = {
            setupMode: "Automatic",
            ownerId: ownerInfo.owner.id,
            ownerModelName: ownerInfo.ownerModelName
        }

        if ( profileIds && profileIds.length > 0 ) 
            tokenData = Object.assign({}, tokenData, {profileIds});

        let token = await this._tokenManager.generateToken(handler, (req.email || req.phone) as string, tokenData, options);

        // send notification
        await this.sendSetupAccessInstructionsToUser(handler, req, token, options)

        // return rsp 
        return rsp;
    }

    private async sendSetupAccessInstructionsToUser(handler: YinzConnection | YinzTransaction, req: Req, token: Token, options?: YinzOptions & {model?: any}): Promise<void> {
    
        // notif
        let notif = new Notif();
        notif.ref = req.reqRef as string;
        notif.reason = "SETUP_ACCESS";
        notif.dest = (req.email || req.phone) as string;
        notif.destType = req.email ? "E" : "S";
        notif.languageCode = 'en' // TODO
        notif.subject = this._notifSubjectTempl || 'setupAccessInstructionsMessageSubject';
        notif.message = this._notifMessageTempl || 'setupAccessInstructionsMessageBody';
        notif.readyOn = new Date();

        // notif templ model data   

        // default model
        let model = {
            notif: notif,
            token: token,
            req: req,
            origin: req.reqLoc,
            setupAccessUrl: req.setupAccessUrl,
        };

        if ( options )
            model = { ...model, ...options.model}
        
        options = {...options, model};

        await this._notifManager.post(handler, notif, options); 

        return;

    }

    protected async createAccessOwner(handler: YinzConnection | YinzTransaction,
        req: Req, rsp: Rsp, options?: YinzOptions): Promise<AccessOwner> {
            
        throw new Exception('ER_ACCESS_SRV__PREP_SETUP_ACCESS_SRV__METH_NOT_IMPL_YET', {
            message: 'This method is notimplemented yet! Sub-classes should implement it!',
        });
    }

}