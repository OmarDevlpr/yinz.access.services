import ServiceTemplate, { DefaultServiceReq, DefaultServiceRsp, ServiceTemplateOptions } from "@yinz/commons.services/ts/ServiceTemplate";
import { TypeormDao } from "@yinz/commons.data";
import User from "@yinz/access.data/models/User";
import { YinzOptions, Asserts, Exception, PasswordUtils } from "@yinz/commons";
import { YinzTransaction, YinzConnection } from "@yinz/commons.data/ts/DataSource";
import LogModel from "@yinz/commons.data/ts/LogModel";

import * as _ from "lodash";

export interface LoginServiceReq extends DefaultServiceReq {
    reqUser: string;
    password?: string;
    passwordDigest?: string;
}

export interface LoginServiceRsp extends DefaultServiceRsp {
    user: User
}

export interface LoginServiceOptions extends ServiceTemplateOptions { 
    userDao: TypeormDao<User>;
    params?: {
        maxWrongAttempt?: number;
    }
};


export default class LoginService extends ServiceTemplate<LoginServiceReq, LoginServiceRsp> {
        
    private _maxWrongAttempts: number;
    private _userDao: TypeormDao<User>;



    constructor(options: LoginServiceOptions) {

        options.serviceName = "LoginService";
        options.logReqRsp = true;

        super(options);

        this._accessManager = options.accessManager;
        this._userDao       = options.userDao;

        // params
        this._maxWrongAttempts = options.params && options.params.maxWrongAttempt || 3;

    }



    protected valReqRemFlds(req: LoginServiceReq, options?: YinzOptions): void {

        Asserts.isNonEmptyString(req.reqUser, 'ER_ACCESS_SRV__LOGIN_SRV__INV_USER_CODE', {
            message: 'The supplied reqUser ' + req.reqUser + ' is invalid! It should be a none-empty string.'
        })

        Asserts.isNonEmptyString(req.password, 'ER_ACCESS_SRV__LOGIN_SRV__INV_PWD', {
            message: 'The supplied password ' + req.password + ' is invalid! It should be a none-empty string.'
        })

    }


    protected async executeService(handler: YinzConnection | YinzTransaction, req: LoginServiceReq, options?: YinzOptions): Promise<LoginServiceRsp> {

            let reqUser = req.reqUser;

            this._logger.trace('About to lookup user "%s"...', reqUser);


            let user: User = await this._accessManager.lookupUserAuthzes(handler, reqUser);

            // 1. check that user exists
            if (user === null) {
                throw new Exception('ER_ACCESS_SRV__LOGIN_SRV__USER_NOT_FOUND', {
                    message: 'User ' + reqUser + ' doesn\'t exist.',
                    code: reqUser,
                });
            }

            // 2. assert that user is active
            if (user.status === 'L') {
                throw new Exception('ER_ACCESS_SRV__LOGIN_SRV__USER_LOCKED', {
                    message: 'The user ' + reqUser + ' is locked.',
                    code: reqUser,
                    user: user,
                });
            }
            if (user.status === 'I') {
                throw new Exception('ER_ACCESS_SRV__LOGIN_SRV__USER_INACTIVE', {
                    message: 'The user ' + reqUser + ' is inactive.',
                    code: reqUser,
                    user: user,
                });
            }
            if (user.wrongAttempts >= this._maxWrongAttempts) {
                throw new Exception('ER_ACCESS_SRV__LOGIN_SRV__WRONG_ATTEMPS_EXCEEDED', {
                    message: 'The user ' + reqUser + ' is inactive.',
                    code: reqUser,
                    user: user,
                });
            }

            // 3. compare passwords

            if (req.password !== user.password) {

                // 3.1 update wrong attempts

                let updateFields: Partial<User> = { wrongAttempts: user.wrongAttempts + 1 };

                if (user.wrongAttempts + 1 >= this._maxWrongAttempts) {
                    updateFields.status = "L";
                }

                await this._userDao.updateById(handler, updateFields, user.id, options)

                throw new Exception('ER_ACCESS_SRV__LOGIN_SRV__WRONG_PWD', {
                    message: 'The password supplied by ' + reqUser + ' is wrong.',
                    code: reqUser,
                    user: user,
                });

            }

            // 4. clear wrong attempts if any

            if ( user.wrongAttempts > 0 ) {
                await this._userDao.updateById(handler, {wrongAttempts: 0}, user.id, options);
            }
            
            let result = { user }
            
            return result;

    }

    protected async translateNegativeRsp(handler: YinzConnection | YinzTransaction, req: LoginServiceReq, rsp: LoginServiceRsp, options?: YinzOptions): Promise<LoginServiceRsp> {
        
        
        const ERR_CODES = ['INV_USER_CODE', 'INV_PWD', 'USER_NOT_FOUND', 'USER_LOCKED', 'WRONG_ATTEMPS_EXCEEDED', 'USER_INACTIVE']

        if (rsp.rspReason && ERR_CODES.indexOf(rsp.rspReason.replace('ER_ACCESS_SRV__LOGIN_SRV__', '')) === -1) {
            rsp.rspReason = 'ER_ACCESS_SRV__LOGIN_SRV__INV_ACCESS';
            rsp.rspMessage = 'Invalid username or password.';
        }

        return rsp;
    }

    protected async logReq(handler: YinzConnection | YinzTransaction, req: LoginServiceReq, options?: YinzOptions): Promise<LogModel> {
        
        // in case the user has provided a passwrod digest, hash it and remove it from the request before it gets logged.

        let hash =  _.isString(req.passwordDigest) && !_.isEmpty(req.passwordDigest) ?
            req.passwordDigest :
            (_.isString(req.reqUser) && !_.isEmpty(req.reqUser) && _.isString(req.password) && !_.isEmpty(req.password) ?
                await PasswordUtils.hashPassword(req.password, req.reqUser) :
                null) as string;

        req.password = hash;

        // req.passwordDigest is not in the model.
        if (req.passwordDigest) {
            delete req.passwordDigest;
        }                

        return super.logReq(handler, req, options);

    }
    
}